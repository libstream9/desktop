#ifndef STREAM9_XDG_DESKTOP_ENTRY_HPP
#define STREAM9_XDG_DESKTOP_ENTRY_HPP

#include "error.hpp"

#include "desktop_entry/entry.hpp"
#include "desktop_entry/key.hpp"
#include "desktop_entry/locale.hpp"
#include "desktop_entry/application_action.hpp"

#include <stream9/array.hpp>
#include <stream9/less.hpp>
#include <stream9/node.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_array.hpp>

#include <cstdint>

namespace stream9::xdg {

/**
 * @model std::move_constructible
 * @model std::destructible
 */
class desktop_entry
{
public:
    template<std::int64_t N>
    using string_list = array<string, N>;

    template<std::int64_t N>
    using string_set = unique_array<string, less, std::identity, N>;

    template<std::int64_t N>
    using string_view_set = unique_array<string_view, less, std::identity, N>;

    using key = desktop_entry_::key;
    using locale = desktop_entry_::locale;
    using entry_group = desktop_entry_::entry_group;

    using entry_group_set = array<entry_group, 1>;
    using entry_group_range = std::ranges::subrange<entry_group_set::const_iterator>;

public:
    // essentials
    desktop_entry(string_view id, string_view path);

    desktop_entry(desktop_entry&&) noexcept;
    desktop_entry& operator=(desktop_entry&&) noexcept;

    ~desktop_entry() noexcept;

    friend void
    swap(desktop_entry& lhs, desktop_entry& rhs) noexcept;

    // specific query
    string_view id() const noexcept;
    string type() const;
    string name(locale const& = {}) const;
    string version() const;
    string generic_name(locale const& = {}) const;
    bool no_display() const noexcept;
    string comment(locale const& = {}) const;
    string icon(locale const& = {}) const;
    bool hidden() const noexcept;

    string_set<5> only_show_in() const;
    string_set<5> not_show_in() const;
    bool should_show_in_current_desktop() const;

    bool dbus_activatable() const noexcept;
    string try_exec() const;

    /**
     * @return unescaped and parsed EXEC entry.
     */
    string_list<5> exec() const;

    string_list<5> exec(string_view filepath) const;

    string_list<5> exec_raw() const; // return raw exec array without expanding special field codes.

    string path() const;
    bool terminal() const noexcept;

    /**
     * @return preference ordered set of mime types
     */
    string_list<5> mime_types() const;

    string_set<5> categories() const;
    string_set<5> implements() const;
    string_set<5> keywords(locale const& = {}) const;

    opt<bool> startup_notify() const noexcept;

    string startup_wm_class() const;
    string url() const;
    bool prefer_non_default_gpu() const noexcept;

    // application action
    array<class desktop_entry_::application_action, 5>
        application_actions() const;

    opt<class desktop_entry_::application_action>
        application_action(string_view name) const noexcept;

    // generic query
    bool contains(key const&) const noexcept;
    string value(key const&) const;

    entry_group_range groups() const noexcept;
    entry_group const* group(string_view name) const noexcept;

    // accessor
    string_view text() const noexcept;   // content of the .desktop file
    string_view origin() const noexcept; // path to the .desktop file

private:
    class impl;
    node<impl> m_p;
};

std::ostream& operator<<(std::ostream&, desktop_entry const&);

} // namespace stream9::xdg

#endif // STREAM9_XDG_DESKTOP_ENTRY_HPP
