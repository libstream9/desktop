#ifndef STREAM9_XDG_APPLICATION_LOCALE_HPP
#define STREAM9_XDG_APPLICATION_LOCALE_HPP

#include "../error.hpp"

#include <stream9/optional.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <cstdint>
#include <iosfwd>

namespace stream9::xdg::desktop_entry_ {

class locale
{
public:
    struct index
    {
        using pos_t = safe_integer<std::int8_t, 0>;
        using length_t = safe_integer<std::int8_t, 0>;

        pos_t lang_pos = 0;
        length_t lang_len = 0;

        pos_t country_pos = 0;
        length_t country_len = 0;

        pos_t encoding_pos = 0;
        length_t encoding_len = 0;

        pos_t modifier_pos = 0;
        length_t modifier_len = 0;

        auto operator<=>(index const&) const noexcept = default;
    };

public:
    locale() = default;

    locale(string_view name);

    locale(opt<string_view> name,
           opt<string_view> lang,
           opt<string_view> country,
           opt<string_view> encoding,
           opt<string_view> modifier );

    template<typename T>
        requires std::convertible_to<T const&, string_view>
    locale(T const& s)
        try : locale { string_view(s) }
    {}
    catch (...) {
        rethrow_error();
    }

    auto begin() const noexcept { return m_name.begin(); }
    auto end() const noexcept { return m_name.end(); }

    bool empty() const noexcept { return m_name.empty(); }

    string_view lang() const noexcept;
    string_view country() const noexcept;
    string_view encoding() const noexcept;
    string_view modifier() const noexcept;

    constexpr auto operator<=>(locale const&) const noexcept = default;
    constexpr bool operator==(locale const&) const noexcept = default;

    template<typename T>
    constexpr auto
    operator<=>(T const& o) const noexcept
        requires requires (string_view const& v1, T const& v2) { v1 <=> v2; }
    {
        return m_name <=> o;
    }

    template<typename T>
    constexpr bool
    operator==(T const& o) const noexcept
        requires requires (string_view const& v1, T const& v2) { v1 == v2; }
    {
        return m_name == o;
    }

    constexpr operator string_view () const noexcept { return m_name; }

    friend std::ostream& operator<<(std::ostream&, locale const&);

private:
    string_view m_name = "";

    index m_index;
};

} // namespace stream9::xdg::desktop_entry_

#endif // STREAM9_XDG_APPLICATION_LOCALE_HPP
