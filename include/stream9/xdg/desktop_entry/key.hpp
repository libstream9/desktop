#ifndef STREAM9_XDG_APPLICATION_KEY_HPP
#define STREAM9_XDG_APPLICATION_KEY_HPP

#include "../error.hpp"

#include "locale.hpp"

#include <stream9/string_view.hpp>

#include <concepts>
#include <iosfwd>

namespace stream9::xdg::desktop_entry_ {

class key
{
public:
    key(string_view);

    template<typename T>
        requires std::convertible_to<T const&, string_view>
    key(T const& s)
        : key { string_view(s) }
    {}

    key(string_view name, class locale const&);

    string_view name() const noexcept { return m_name; }
    class locale const& locale() const noexcept { return m_locale; }

    bool is_localized() const noexcept { return !m_locale.empty(); }

    constexpr auto operator<=>(key const&) const noexcept = default;
    constexpr bool operator==(key const&) const noexcept = default;

    template<typename T>
    constexpr auto
    operator<=>(T const& o) const noexcept
        requires requires (string_view const& v1, T const& v2) { v1 <=> v2; }
    {
        return m_name <=> o;
    }

    template<typename T>
    constexpr bool
    operator==(T const& o) const noexcept
        requires requires (string_view const& v1, T const& v2) { v1 == v2; }
    {
        return m_name == o;
    }

    friend std::ostream& operator<<(std::ostream&, key const&);

private:
    string_view m_name;
    class locale m_locale;
};

} // namespace stream9::xdg::desktop_entry_

#endif // STREAM9_XDG_APPLICATION_KEY_HPP
