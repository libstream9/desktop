#ifndef STREAM9_XDG_APPLICATION_V1_DATABASE_UPDATER_HPP
#define STREAM9_XDG_APPLICATION_V1_DATABASE_UPDATER_HPP

#include "database.hpp"

#include "../../error.hpp"

#include <stream9/linux/fd.hpp>
#include <stream9/node.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::xdg::applications::v1 {

class database_updater
{
    struct impl;
public:
    class event_handler;

public:
    database_updater(database&);
    ~database_updater() noexcept;

    lx::fd_ref monitor_fd() const noexcept;

    safe_integer<int, 0> process_events();

    void add_event_handler(event_handler&);

    class event_handler
    {
    public:
        event_handler() noexcept = default;
        ~event_handler() noexcept;

        event_handler(event_handler const&) = delete;
        event_handler& operator=(event_handler const&) = delete;
        event_handler(event_handler&&) = delete;
        event_handler& operator=(event_handler&&) = delete;

        virtual void desktop_entry_added(string_view /*id*/, string_view /*path*/) noexcept {}
        virtual void desktop_entry_changed(string_view /*id*/, string_view /*path*/) noexcept {}
        virtual void desktop_entry_deleted(string_view /*id*/) noexcept {}

    private:
        friend struct impl;
        impl* m_src = nullptr;
    };

private:
    node<impl> m_p;
};

} // namespace stream9::xdg::applications::v1

#endif // STREAM9_XDG_APPLICATION_V1_DATABASE_UPDATER_HPP
