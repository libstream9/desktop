#ifndef STREAM9_XDG_APPLICATION_DATABASE_HPP
#define STREAM9_XDG_APPLICATION_DATABASE_HPP

#include "../../error.hpp"
#include "../../namespace.hpp"
#include "../../desktop_entry.hpp"

#include <stream9/array.hpp>
#include <stream9/node.hpp>
#include <stream9/optional.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_array.hpp>

#include <exception>

namespace stream9::xdg::mime { class database; }

namespace stream9::xdg::applications::v1 {

/**
 * @model std::default_initializable
 * @model std::move_constructible
 * @model std::destructible
 */
class database
{
public:
    using preference_ordered_desktop_entry_set = array<desktop_entry, 10>;
    using desktop_entry_set = array<desktop_entry>;
    using error_set = array<std::exception_ptr>;

public:
    database();
    database(xdg::mime::database const&);

    database(database&&) noexcept;
    database& operator=(database&&) noexcept;

    ~database() noexcept;

    /**
     * @return preference ordered set of desktop entries which are associated
     *         with given mime type or its less specific types.
     */
    preference_ordered_desktop_entry_set
        find_applications(string_view mime_type) const;

    /**
     * @return default application which is associated with given mime type
     *         or its less specific types.
     */
    opt<desktop_entry>
        find_default_application(string_view mime_type) const;

    opt<desktop_entry>
        find_default_application(string_view mime_type,
                                 error_set& errors) const;

    /**
     * @return set of all applications registered to this database.
     */
    desktop_entry_set
        all_applications() const;

    /**
     * prefix prefix of mime type to query
     */
    unique_array<string_view>
        associated_mime_types(string_view prefix = "") const;

    array<string_view>
        data_directory_paths() const;

    opt<desktop_entry>
        load_desktop_entry(string_view id) const noexcept;

    /* modifiers */
    void add_association(string_view mime_type, string_view id);

    void remove_association(string_view mime_type, string_view id);

    void add_default_application(string_view mime_type, string_view id);

    void remove_default_application(string_view mime_type, string_view id);

    void save();

private:
    friend class database_updater;

    class impl;
    node<impl> m_p;
};

} // namespace stream9::xdg::applications::v1

#endif // STREAM9_XDG_APPLICATION_DATABASE_HPP
