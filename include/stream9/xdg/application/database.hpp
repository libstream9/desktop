#ifndef STREAM9_XDG_APPLICATION_DATABASE_HPP
#define STREAM9_XDG_APPLICATION_DATABASE_HPP

#include <stream9/xdg/desktop_entry.hpp>

#include <stream9/array.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime { class database; }

namespace stream9::xdg::applications::inline v2 {

class database
{
public:
    // essentials
    database(xdg::mime::database const&);

    ~database() noexcept;

    database(database const&) = delete;
    database& operator=(database const&) = delete;
    database(database&&) noexcept = default;
    database& operator=(database&&) noexcept = default;

    // query
    /**
     * @return preference ordered set of desktop entries which are associated
     *         with given mime type or its less specific types.
     */
    array<desktop_entry>
        find_applications(string_view mime_type) const;

    /**
     * @return default application which is associated with given mime type
     *         or its less specific types.
     */
    opt<desktop_entry>
        find_default_application(string_view mime_type) const;

    opt<desktop_entry>
        load_desktop_entry(string_view id) const noexcept;

    /**
     * @return set of all applications registered to this database.
     */
    array<desktop_entry>
        all_applications() const;

    unique_array<string/*mime_type*/>
        associated_mime_types(string_view mime_type_prefix = "") const;

    // modifiers
    void refresh();

private:
    friend class database_updater;

    struct impl;
    node<impl> m_p;
};

} // namespace stream9::xdg::applications::inline v2

#endif // STREAM9_XDG_APPLICATION_DATABASE_HPP
