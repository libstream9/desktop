#ifndef STREAM9_DESKTOP_NAMESPACE_HPP
#define STREAM9_DESKTOP_NAMESPACE_HPP

namespace stream9::xdg {

namespace st9 { using namespace stream9; }

} // namespace stream9::xdg

#endif // STREAM9_DESKTOP_NAMESPACE_HPP
