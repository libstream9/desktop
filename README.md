# stream9::desktop

A C++20 implementation of freedesktop.org Desktop Entry Specification.

## Based Specification

- [Desktop Entry Specification version 1.5](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-1.5.html)
- [Association between MIME types and applications version 1.0.1](https://specifications.freedesktop.org/mime-apps-spec/mime-apps-spec-1.0.1.html)
