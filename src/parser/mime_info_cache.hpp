#ifndef STREAM9_DESKTOP_PARSER_MIME_INFO_CACHE_HPP
#define STREAM9_DESKTOP_PARSER_MIME_INFO_CACHE_HPP

#include "../parser.hpp"

#include <stream9/array.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_table.hpp>

#include <system_error>

namespace stream9::xdg::parser::mime_info_cache {

using mime_desktop_file_id_map = unique_table<string_view, string_view>;

void parse(string_view text,
           mime_desktop_file_id_map& entries,
           array<error>* errors = nullptr);

enum class errc {
    ok = 0,
    missing_required_group,
    invalid_group_name,
    duplicated_group,
    duplicated_key,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::xdg::parser::mime_info_cache

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::parser::mime_info_cache::errc>
    : std::true_type {};

} // namespace std

#endif // STREAM9_DESKTOP_PARSER_MIME_INFO_CACHE_HPP
