#include "exec_string.hpp"

#include "../parser.hpp"
#include "../scanner/exec_string.hpp"
#include "namespace.hpp"

#include <stream9/push_back.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::parser::exec_string {

array<string, 5>
parse(string_view exec, location_tracker const& locs)
{
    array<string, 5> result;

    class handler
    {
    public:
        using next_action = scanner::next_action;
        using iterator = scanner::exec_string::iterator;

    public:
        handler(string_view text,
                location_tracker const& locs,
                array<string, 5>& result)
            : m_text { text }
            , m_locs { locs }
            , m_result { result }
        {}

        void begin_argument(next_action&) {}

        void end_argument(next_action&)
        {
            st9::push_back(m_result, std::move(m_argument));
            m_argument = "";
        }

        void on_character(char c, next_action&)
        {
            m_argument.append(c);
        }

        void on_error(std::error_code const& ec, iterator it, next_action&)
        {
            auto loc = m_locs.get_location(it);

            throw_error("parse error", ec, {
                { "line", loc.line },
                { "column", loc.col },
            });
        }

    private:
        string_view m_text;
        location_tracker const& m_locs;
        array<string, 5>& m_result;
        string m_argument;
    } h { exec, locs, result };

    scanner::exec_string::scan(exec, h);

    return result;
}

} // namespace stream9::xdg::parser::exec_string
