#include "desktop_entry.hpp"

#include "namespace.hpp"
#include "parser/exec_string.hpp"
#include "scanner/desktop_entry_key.hpp"

#include <stream9/xdg/desktop_entry/entry.hpp>
#include <stream9/xdg/desktop_entry/locale.hpp>
#include <stream9/xdg/desktop_entry/escape_sequence.hpp>

#include <stream9/array_view.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/less.hpp>
#include <stream9/optional.hpp>
#include <stream9/push_back.hpp>
#include <stream9/ranges/contains.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/ranges/iterator_at.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/strings/starts_with.hpp>
#include <stream9/strings/substr.hpp>
#include <stream9/strings/split.hpp>
#include <stream9/unique_array.hpp>
#include <stream9/xdg/settings/scanner.hpp>

#include <algorithm>
#include <cctype>
#include <system_error>

namespace stream9::xdg::parser::desktop_entry {

using desktop_entry_::entry;
using desktop_entry_::entry_group;
using desktop_entry_::key;

using entry_set = entry_group::entry_set;

std::error_category&
error_category()
{
    struct impl : std::error_category {
        char const* name() const noexcept override
        {
            return "parser::desktop_entry";
        }

        std::string message(int const ec) const override
        {
            switch (static_cast<errc>(ec)) {
                case errc::unknown_key:
                    return "unknown key";
                case errc::duplicated_key:
                    return "duplicated key";
                case errc::duplicated_group:
                    return "duplicated group";
                case errc::missing_required_group:
                    return "missing required group";
                case errc::missing_required_entry:
                    return "missing required entry";
                case errc::missing_registered_action_group:
                    return "missing registered action group";
                case errc::localized_key_on_string_entry:
                    return "localized key on string entry";
                case errc::localized_key_on_boolean_entry:
                    return "localized key on boolean entry";
                case errc::unknown_desktop_entry_type:
                    return "unknown desktop entry type";
                case errc::unknown_desktop_entry_key:
                    return "unknown desktop entry key";
                case errc::invalid_value:
                    return "invalid value";
                case errc::invalid_boolean_value:
                    return "invalid boolean value";
                case errc::duplicated_action_key:
                    return "duplicated action key";
                case errc::unregistered_action_group:
                    return "unregistered action group";
                case errc::entry_type_mismatch:
                    return "entry type mismatch";
                case errc::localized_entry_without_default:
                    return "localized entry without default";
                default:
                    return "unknown error";
            }
        }
    };

    static impl impl_;

    return impl_;
}

enum class desktop_entry_type {
    unknown = 0,
    application = 1,
    link = 2,
    directory = 3,
};

enum class value_type : int8_t {
    string,
    locale_string,
    icon_string,
    boolean,
    numeric,
};

struct key_definition {
    char const* name;
    value_type type;
    bool required : 1;
    bool application : 1;
    bool link : 1;
    bool directory : 1;
};
static_assert(sizeof(key_definition) == 16);

static key_definition key_tbl[] = {
    { "Actions",              value_type::string,        false,  true, false, false },
    { "Categories",           value_type::string,        false,  true, false, false },
    { "Comment",              value_type::locale_string, false,  true,  true,  true },
    { "DBusActivatable",      value_type::boolean,       false,  true, false, false },
    { "Exec",                 value_type::string,        false,  true, false, false },
    { "GenericName",          value_type::locale_string, false,  true,  true,  true },
    { "Hidden",               value_type::boolean,       false,  true,  true,  true },
    { "Icon",                 value_type::icon_string,   false,  true,  true,  true },
    { "Implements",           value_type::string,        false,  true, false, false },
    { "Keywords",             value_type::locale_string, false,  true, false, false },
    { "MimeType",             value_type::string,        false,  true, false, false },
    { "Name",                 value_type::locale_string,  true,  true,  true,  true },
    { "NoDisplay",            value_type::boolean,       false,  true,  true,  true },
    { "NotShowIn",            value_type::string,        false,  true,  true,  true },
    { "OnlyShowIn",           value_type::string,        false,  true,  true,  true },
    { "Path",                 value_type::string,        false,  true, false, false },
    { "PrefersNonDefaultGPU", value_type::boolean,       false,  true, false, false },
    { "StartupNotify",        value_type::boolean,       false,  true, false, false },
    { "StartupWMClass",       value_type::string,        false,  true, false, false },
    { "Terminal",             value_type::boolean,       false,  true, false, false },
    { "TryExec",              value_type::string,        false,  true, false, false },
    { "Type",                 value_type::string,         true,  true,  true,  true },
    { "URL",                  value_type::string,         true, false,  true, false },
    { "Version",              value_type::string,        false,  true,  true,  true },
};

static string_view
get_group_name(entry_group const& g)
{
    return g.name();
}

static array_view<string_view const>
required_desktop_entry_keys(desktop_entry_type const type)
{
    static string_view const keys[] = { "Name", "URL" };

    if (type == desktop_entry_type::link) {
        return keys;
    }
    else if (type == desktop_entry_type::unknown) {
        return { keys, 0, 0 }; //empty
    }
    else {
        return { keys, 0, 1 };
    }
}

static key_definition const*
find_key_definition(string_view const key)
{
    auto const it = std::ranges::lower_bound(key_tbl, key, {},
        [](auto&& def) { return def.name; });

    if (it == rng::end(key_tbl) || it->name != key) {
        return nullptr;
    }
    else {
        return &*it;
    }
}

static bool
is_boolean_value(string_view const s)
{
    return s == "true"
        || s == "false";
}

static bool
is_extended_key(string_view s)
{
    return str::starts_with(s, "X-");
}

static string_view
action_group_prefix()
{
    return "Desktop Action ";
}

static bool
is_action_group(entry_group& g)
{
    return str::starts_with(g.name(), action_group_prefix());
}

static bool
is_action_group_registered(entry_group& g, auto const& actions)
{
    //auto const& name = g.name().substr(action_group_prefix().size());
    auto name = str::substr(g.name(), action_group_prefix().size());

    return rng::contains(actions, name);
}

static bool
is_allowed_for_entry_type(
    key_definition const& key, desktop_entry_type const type)
{
    switch (type) {
        case desktop_entry_type::application:
            return key.application;
        case desktop_entry_type::link:
            return key.link;
        case desktop_entry_type::directory:
            return key.directory;
        default:
            break;
    }

    return false;
}

using namespace stream9::xdg; //TODO remove later

class desktop_entry_scanner
{
public:
    void run(string_view text,
             array<entry_group, 1>& groups,
             location_tracker& locs,
             array<error>* errors/*= nullptr*/)
    {
        m_entry_groups = &groups;
        m_locs = &locs;
        m_errors = errors;

        m_locs->new_line(text.begin());

        xdg::settings::scan(text, *this);

        push_current_entry_group();
    }

    void on_group_header(string_view s, settings::next_action&)
    {
        push_current_entry_group();

        m_group_name = s;
    }

    void on_entry(string_view key_,
                  string_view value, settings::next_action&)
    {
        m_name = {};
        m_locale = {};
        m_lang = {};
        m_country = {};
        m_encoding = {};
        m_modifier = {};

        if (!scanner::desktop_entry_key::scan(key_, *this)) return;

        auto [_, ok] = m_entry_set.insert({
            key {
                *m_name,
                { m_locale, m_lang, m_country, m_encoding, m_modifier }
            },
            value
        });

        if (!ok) {
            push_error(errc::duplicated_key, *m_name);
        }
    }

    void on_name(string_view const s, scanner::next_action&)
    {
        m_name = s;
    }

    void on_locale(string_view const s, scanner::next_action&)
    {
        m_locale = s;
    }

    void on_lang(string_view const s, scanner::next_action&)
    {
        m_lang = s;
    }

    void on_country(string_view const s, scanner::next_action&)
    {
        m_country = s;
    }

    void on_encoding(string_view const s, scanner::next_action&)
    {
        m_encoding = s;
    }

    void on_modifier(string_view const s, scanner::next_action&)
    {
        m_modifier = s;
    }

    void on_comment(string_view, settings::next_action&)
    {}

    void on_error(std::error_code const& ec,
                  string_view const range, settings::next_action&)
    {
        push_error(ec, range);
    }

    void on_error(std::error_code const& ec,
                  string_view const range, scanner::next_action&)
    {
        push_error(ec, range);
    }

    void on_new_line(string_view::iterator it)
    {
        m_locs->new_line(it + 1);
    }

private:
    void push_current_entry_group()
    {
        if (!m_group_name) return;

        st9::emplace_back(*m_entry_groups,
            *m_group_name,
            std::move(m_entry_set)
        );

        m_group_name = {};
        m_entry_set = {};
    }

    void push_error(std::error_code const& ec,
                    string_view range,
                    opt<string_view> extra_info = {})
    {
        if (!m_errors) return;

        auto const loc = m_locs->get_location(range.begin());

        json::object cxt {
            { "line", loc.line },
            { "column", loc.col },
        };
        if (extra_info) {
            cxt["detail"] = *extra_info;
        }
        st9::emplace_back(*m_errors, "parse error", ec, std::move(cxt));
    }

    void push_error(errc const e,
                    string_view range,
                    opt<string_view> extra_info = {})
    {
        push_error(make_error_code(e), range, extra_info);
    }

private:
    opt<string_view> m_group_name;
    opt<string_view> m_name;
    opt<string_view> m_locale;
    opt<string_view> m_lang;
    opt<string_view> m_country;
    opt<string_view> m_encoding;
    opt<string_view> m_modifier;

    entry_set m_entry_set;

    array<entry_group, 1>* m_entry_groups = nullptr;
    array<error>* m_errors = nullptr;
    location_tracker* m_locs = nullptr;
};

static bool
is_dbus_activatable(entry_group const& g)
{
    auto const it = g.find("DBusActivatable");

    return it != g.end() && it->value == "true";
}

class semantics_checker
{
public:
    semantics_checker(array<entry_group, 1>& groups,
                      location_tracker& locs,
                      array<error>* errors)
        : m_entry_groups { groups }
        , m_locs { locs }
        , m_errors { errors }
    {}

    void run()
    {
        sort_and_unique_entry_groups();

        hoist_desktop_entry_group();

        assert(!m_entry_groups.empty());
        auto& desktop_entry_group = m_entry_groups.front();

        check_desktop_entry_group(desktop_entry_group);

        auto actions = get_action_names(desktop_entry_group);
        auto dbus_activatable = is_dbus_activatable(desktop_entry_group);

        auto it = std::remove_if(
            rng::iterator_at(m_entry_groups, 1), m_entry_groups.end(),
            [&](auto& group) {
                if (is_action_group(group)) {
                    if (!is_action_group_registered(group, actions)) {
                        push_error(errc::unregistered_action_group, group.name());
                        return true;
                    }

                    return !check_action_entry_group(group, dbus_activatable);
                }
                else {
                    return !check_misc_entry_group(group);
                }
            } );

        m_entry_groups.erase(it, m_entry_groups.end());

        for (auto const& name: actions) {
            if (!has_action_group(name)) {
                push_error(errc::missing_registered_action_group, name);
            }
        }
    }

private:
    void sort_and_unique_entry_groups()
    {
        std::ranges::stable_sort(m_entry_groups, {}, get_group_name );

        auto duplicates = std::ranges::unique(m_entry_groups, {}, get_group_name);

        for (auto const& g: duplicates) {
            push_error(errc::duplicated_group, g.name());
        }

        m_entry_groups.erase(duplicates.begin(), duplicates.end());
    }

    void hoist_desktop_entry_group()
    {
        string_view name = "Desktop Entry";

        auto it = std::ranges::lower_bound(m_entry_groups, name, {}, get_group_name);

        if (it == m_entry_groups.end() || it->name() != name) {
            throw_error(errc::missing_required_group, {}, name);
        }

        std::rotate(m_entry_groups.begin(), it, it + 1); // hoist up to front
    }

    void check_desktop_entry_group(entry_group& group)
    {
        using std::ranges::binary_search;

        auto type = get_desktop_entry_type(group);

        st9::erase_if(group,
            [&](auto&& ent) {
                return !check_desktop_entry_entry(ent, type);
            } );

        check_localized_entry_has_corresponding_default(group);

        for (auto const& key: required_desktop_entry_keys(type)) {
            auto found = binary_search(group, key, {},
                               [&](auto&& ent) { return ent.key.name(); } );
            if (!found) {
                throw_error(errc::missing_required_entry,
                            group.name(), string(key) );
            }
        }

        check_conditionary_required_entries(group, type);
    }

    desktop_entry_type get_desktop_entry_type(entry_group const& group)
    {
        auto it = group.find("Type");
        if (it == group.end()) {
            throw_error(errc::missing_required_entry, group.name(), "Type");
        }

        auto const& value = it->value;

        if (value == "Application") {
            return desktop_entry_type::application;
        }
        else if (value == "Link") {
            return desktop_entry_type::link;
        }
        else if (value == "Directory") {
            return desktop_entry_type::directory;
        }
        else {
            push_error(errc::unknown_desktop_entry_type, value);
            return desktop_entry_type::unknown;
        }
    }

    void check_localized_entry_has_corresponding_default(entry_group& group)
    {
        unique_array<string_view, less, std::identity, 30> names_of_localized_entry;

        for (auto const& entry: group) {
            if (entry.key.is_localized()) {
                names_of_localized_entry.insert(entry.key.name());
            }
        }

        array<string_view, 30> names_with_no_default_entry {
            std::ranges::views::filter(
                names_of_localized_entry,
                [&](auto&& n) {
                    return !group.contains(n);
                })
        };

        for (auto const& name: names_with_no_default_entry) {
            st9::erase_if(group,
                [&](auto&& e) {
                    if (e.key.name() == name) {
                        push_error(errc::localized_entry_without_default, e.key.locale());
                        return true;
                    }
                    else {
                        return false;
                    }
                } );
        }
    }

    bool check_desktop_entry_entry(entry const& e, desktop_entry_type e_type)
    {
        if (e_type == desktop_entry_type::unknown) return true;

        auto const& name = e.key.name();

        if (name == "Type") return true;

        auto* const def = find_key_definition(name);
        if (def == nullptr) {
            push_error(errc::unknown_desktop_entry_key, e.key.name());
            return true;
        }

        switch (def->type) {
            case value_type::string:
                if (!is_string_entry(e)) {
                    return false;
                }
                break;
            case value_type::boolean:
                if (!is_boolean_entry(e)) {
                    return false;
                }
                break;
            case value_type::locale_string:
            case value_type::icon_string:
            default:
                break;
        }

        if (!is_allowed_for_entry_type(*def, e_type)) {
            push_error(errc::entry_type_mismatch, e.key.name());
            return false;
        }

        return true;
    }

    bool is_valid_action_name(string_view text)
    {
        bool ok = true;

        auto it = text.begin(), end = text.end();
        for (; it != end; ++it) {
            auto c = *it;
            if (!(std::isalnum(c) || c == '-')) {
                push_error(errc::invalid_value, { it, it + 1 });
                ok = false;
            }
        }

        return ok;
    }

    bool check_action_entry_group(entry_group& group,
                                  bool dbus_activatable)
    {
        bool has_name = false;
        bool has_exec = false;
        bool result = true;

        check_localized_entry_has_corresponding_default(group);

        st9::erase_if(group,
            [&](auto&& entry) {
                if (!check_action_entry(entry)) {
                    return true;
                }

                if (!has_name && entry.key.name() == "Name") {
                    has_name = true;
                }

                if (!has_exec && entry.key == "Exec") {
                    has_exec = true;
                }

                return false;
            } );

        if (!has_name) {
            push_error(errc::missing_required_entry, group.name(), "Name");
            result = false;
        }

        if (!dbus_activatable && !has_exec) {
            push_error(errc::missing_required_entry, group.name(), "Exec");
            result = false;
        }

        return result;
    }

    bool check_misc_entry_group(entry_group&)
    {
        return true;
    }

    void check_conditionary_required_entries(
                            entry_group& group,
                            desktop_entry_type type)
    {
        if (type == desktop_entry_type::application) {
            if (!is_dbus_activatable(group)) {
                auto it = group.find("Exec");
                if (it == group.end()) {
                    throw_error(
                        errc::missing_required_entry, group.name(), "Exec");
                }
                else {
                    parser::exec_string::parse(unescape(it->value), m_locs);
                }
            }
        }
    }

    bool check_action_entry(entry const& e)
    {
        auto const& key = e.key.name();
        if (key == "Name" || key == "Icon") {
            return true;
        }
        else if (key == "Exec") {
            return is_string_entry(e);
        }
        else if (is_extended_key(key)) {
            return true;
        }
        else {
            push_error(errc::unknown_key, key);
            return true;
        }
    }

    bool is_string_entry(entry const& e)
    {
        if (e.key.is_localized()) {
            push_error(errc::localized_key_on_string_entry, e.key.locale());
            return false;
        }

        return true;
    }

    bool is_boolean_entry(entry const& e)
    {
        bool rv = true;

        if (e.key.is_localized()) {
            push_error(errc::localized_key_on_boolean_entry, e.key.locale());
            rv = false;
        }

        if (!is_boolean_value(e.value)) {
            push_error(errc::invalid_boolean_value, e.value);
            rv = false;
        }

        return rv;
    }

    array<string_view, 5>
        get_action_names(entry_group const& group)
    {
        array<string_view, 5> result;

        auto it = group.find("Actions");
        if (it != group.end()) {
            result = str::split(it->value, ";");
        }

        std::ranges::stable_sort(result);

        auto duplicates = std::ranges::unique(result);
        for (auto const& name: duplicates) {
            push_error(errc::duplicated_action_key, name);
        }
        result.erase(duplicates.begin(), duplicates.end());

        auto invalids = std::ranges::remove_if(result,
            [&](auto&& n) { return !is_valid_action_name(n); });

        result.erase(invalids.begin(), invalids.end());

        return result;
    }

    bool has_action_group(string_view name) const
    {
        auto prefix = "Desktop Action ";

        for (auto const& group: m_entry_groups) {
            if (str::starts_with(group.name(), prefix)) {
                safe_integer<ptrdiff_t, 0> idx = str::size(prefix);
                if (str::substr(group.name(), idx) == name) {
                    return true;
                }
            }
        }

        return false;
    }

    void push_error(errc e, string_view range,
                    opt<string_view> extra_info = {})
    {
        if (m_errors) {
            st9::push_back(*m_errors, make_error(e, range, extra_info));
        }
    }

    void throw_error(errc e,
                     opt<string_view> range,
                     opt<string_view> extra_info = {})
    {
        throw make_error(e, range, extra_info);
    }

    error make_error(errc e,
                     opt<string_view> range,
                     opt<string_view> extra_info = {})
    {
        auto loc = !range ? location() : m_locs.get_location(range->begin());

        json::object cxt {
            { "line", loc.line },
            { "column", loc.col },
        };
        if (extra_info) {
            cxt["detail"] = *extra_info;
        }
        return { "parse error", make_error_code(e), std::move(cxt) };
    }

private:
    array<entry_group, 1>& m_entry_groups;
    location_tracker& m_locs;
    array<error>* m_errors = nullptr;
};

static void
scan_text(string_view const text,
          array<entry_group, 1>& groups,
          location_tracker& locs,
          array<error>* const errors)
{
    desktop_entry_scanner s;

    s.run(text, groups, locs, errors);
}

static void
check_semantics(array<entry_group, 1>& groups,
                location_tracker& locs,
                array<error>* const errors)
{
    semantics_checker s { groups, locs, errors };

    s.run();
}

void
parse(string_view text,
      array<entry_group, 1>& groups,
      array<error>* const errors/*= nullptr*/)
{
    location_tracker locs;

    scan_text(text, groups, locs, errors);

    check_semantics(groups, locs, errors);
}

} // namespace stream9::xdg::parser::desktop_entry
