#include "mime_apps_list.hpp"

#include "../namespace.hpp"

#include <stream9/emplace_back.hpp>
#include <stream9/strings/split.hpp>
#include <stream9/xdg/settings/scanner.hpp>

namespace stream9::xdg::parser::mime_apps_list {

class parser
{
public:
    using next_action = settings::next_action;

public:
    void run(string_view text,
             entry_map& added_assocs,
             entry_map& removed_assocs,
             entry_map& default_apps,
             array<error>* errors)
    {
        try {
            m_added_assocs = &added_assocs;
            m_removed_assocs = &removed_assocs;
            m_default_apps = &default_apps;
            m_errors = errors;

            m_locs = {};
            m_locs.new_line(text.begin());

            settings::scan(text, *this);
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_group_header(string_view text, next_action& a)
    {
        try {
            if (text == "Added Associations") {
                m_group = group::added_associations;
            }
            else if (text == "Removed Associations") {
                m_group = group::removed_associations;
            }
            else if (text == "Default Applications") {
                m_group = group::default_applications;
            }
            else {
                push_error(errc::invalid_group_name, text);
                a = next_action::skip_group;
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_entry(string_view mime_type,
                  string_view desktop_file_ids, next_action& a)
    {
        try {
            if (m_group == group::added_associations) {
                auto [_, ok] =
                    m_added_assocs->emplace(
                        string(mime_type), parse_ids(desktop_file_ids) );
                if (!ok) {
                    push_error(errc::duplicated_key, mime_type);
                }
            }
            else if (m_group == group::removed_associations) {
                auto [_, ok] =
                    m_removed_assocs->emplace(
                        string(mime_type), parse_ids(desktop_file_ids) );
                if (!ok) {
                    push_error(errc::duplicated_key, mime_type);
                }
            }
            else if (m_group == group::default_applications) {
                auto [_, ok] =
                    m_default_apps->emplace(
                        string(mime_type), parse_ids(desktop_file_ids) );
                if (!ok) {
                    push_error(errc::duplicated_key, mime_type);
                }
            }
            else {
                push_error(errc::missing_required_group, mime_type);
                a = next_action::skip_group;
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_comment(string_view, next_action&) {}

    void on_error(std::error_code const& ec, string_view range,
                  next_action&)
    {
        try {
            push_error(ec, range);
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_new_line(string_view::iterator it)
    {
        try {
            m_locs.new_line(it + 1);
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    id_set parse_ids(string_view ids) const
    {
        return str::split(ids, ";");
    }

    void push_error(std::error_code const& ec,
                    string_view range,
                    opt<string_view> extra_info = {})
    {
        try {
            if (!m_errors) return;

            auto loc = m_locs.get_location(range.begin());

            json::object cxt {
                { "line", loc.line },
                { "column", loc.col },
            };
            if (extra_info) {
                cxt["detail"] = *extra_info;
            }

            st9::emplace_back(*m_errors, "parse error", ec, std::move(cxt));
        }
        catch (...) {
            rethrow_error();
        }
    }

    void push_error(errc e, string_view range,
                    opt<string_view> extra_info = {})
    {
        push_error(make_error_code(e), range, extra_info);
    }

private:
    enum class group {
        none,
        added_associations,
        removed_associations,
        default_applications
    };

    group m_group = group::none;
    entry_map* m_added_assocs;
    entry_map* m_removed_assocs;
    entry_map* m_default_apps;
    array<error>* m_errors = nullptr;
    location_tracker m_locs;
};

std::error_category const&
error_category()
{
    struct impl : std::error_category {
        char const* name() const noexcept override
        {
            return "parser::mime_apps_list";
        }

        std::string message(int e) const override
        {
            switch (static_cast<errc>(e)) {
                case errc::ok:
                    return "ok";
                case errc::invalid_group_name:
                    return "invalid group name";
                case errc::duplicated_key:
                    return "duplicated key";
                case errc::missing_required_group:
                    return "missing required group";
                default:
                    return "unknown error";
            }
        }
    };

    static impl instance;

    return instance;
}

void
parse(string_view text,
      entry_map& added_assocs,
      entry_map& removed_assocs,
      entry_map& default_applications,
      array<error>* errors/*= nullptr*/)
{
    try {
        parser p;

        p.run(text, added_assocs, removed_assocs, default_applications, errors);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::parser::mime_apps_list
