#ifndef STREAM9_DESKTOP_PARSER_MIME_APPS_LIST_HPP
#define STREAM9_DESKTOP_PARSER_MIME_APPS_LIST_HPP

#include "../parser.hpp"
#include "../application/id_set.hpp"

#include <stream9/array.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_table.hpp>

#include <system_error>

namespace stream9::xdg::parser::mime_apps_list {

using applications::id_set;
using entry_map = unique_table<string, id_set>;

void parse(string_view text,
           entry_map& added_assocs,
           entry_map& removed_assocs,
           entry_map& default_applications,
           array<error>* errors = nullptr);

enum class errc {
    ok = 0,
    invalid_group_name,
    duplicated_key,
    missing_required_group,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::xdg::parser::mime_apps_list

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::parser::mime_apps_list::errc>
    : std::true_type {};

} // namespace std

#endif // STREAM9_DESKTOP_PARSER_MIME_APPS_LIST_HPP
