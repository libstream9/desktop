#ifndef STREAM9_DESKTOP_PARSER_HPP
#define STREAM9_DESKTOP_PARSER_HPP

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::parser {

struct location {
    using line_t = stream9::safe_integer<int, 1>;
    using column_t = stream9::safe_integer<int, 0>;

    bool operator==(location const&) const = default;
    friend std::ostream& operator<<(std::ostream&, location const&);

    line_t line;
    column_t col;
};

class location_tracker
{
public:
    using iterator = string_view::iterator;

public:
    void new_line(iterator);

    location get_location(iterator) const noexcept;

private:
    array<iterator> m_bols;
};

} // namespace stream9::xdg::parser

#endif // STREAM9_DESKTOP_PARSER_HPP
