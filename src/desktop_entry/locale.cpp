#include <stream9/xdg/desktop_entry/locale.hpp>

#include "scanner/locale.hpp"

#include <stream9/strings/substr.hpp>

namespace stream9::xdg::desktop_entry_ {

using scanner::next_action;

static locale::index
make_index(opt<string_view> locale,
           opt<string_view> lang,
           opt<string_view> country,
           opt<string_view> encoding,
           opt<string_view> modifier )
{
    try {
        locale::index result {};
        if (!locale) {
            return result;
        }

        result.lang_pos = lang->begin() - locale->begin();
        result.lang_len = lang->size();

        auto last = lang->end() - locale->begin();

        if (country) {
            result.country_pos = country->begin() - locale->begin();
            result.country_len = country->size();

            last = country->end() - locale->begin();
        }
        else {
            result.country_pos = last;
        }

        if (encoding) {
            result.encoding_pos = encoding->begin() - locale->begin();
            result.encoding_len = encoding->size();

            last = encoding->end() - locale->begin();
        }
        else {
            result.encoding_pos = last;
        }

        if (modifier) {
            result.modifier_pos = modifier->begin() - locale->begin();
            result.modifier_len = modifier->size();

            last = modifier->end() - locale->begin();
        }
        else {
            result.modifier_pos = last;
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

class locale_parser
{
public:
    locale::index run(string_view text)
    {
        try {
            m_locale = text;

            if (!scanner::locale::scan(text, *this)) {
                throw_error(errc::parse_error, {
                    { "detail", "invalid locale" },
                });
            }

            return make_index(*m_locale, *m_lang, m_country, m_encoding, m_modifier);
        }
        catch (...) {
            rethrow_error({
                { "text", text }
            });
        }
    }

    void on_locale(string_view, next_action&) {}

    void on_lang(string_view s, next_action&) noexcept
    {
        m_lang = s;
    }

    void on_country(string_view s, next_action&) noexcept
    {
        m_country = s;
    }

    void on_encoding(string_view s, next_action&) noexcept
    {
        m_encoding = s;
    }

    void on_modifier(string_view s, next_action&) noexcept
    {
        m_modifier = s;
    }

    void on_error(std::error_code const&,
                  string_view, next_action&)
    {}

private:
    opt<string_view> m_locale;
    opt<string_view> m_lang;
    opt<string_view> m_country;
    opt<string_view> m_encoding;
    opt<string_view> m_modifier;
};

/*
 * locale
 */
locale::
locale(string_view name)
    : m_name { name }
{
    try {
        if (m_name.empty()) return;

        locale_parser p;
        m_index = p.run(name);
    }
    catch (...) {
        rethrow_error();
    }
}

locale::
locale(opt<string_view> name,
       opt<string_view> lang,
       opt<string_view> country,
       opt<string_view> encoding,
       opt<string_view> modifier)
    try : m_name { name ? *name : "" }
    , m_index { make_index(name, lang, country, encoding, modifier) }
{}
catch (...) {
    rethrow_error();
}

string_view locale::
lang() const noexcept
{
    return str::substr(m_name, m_index.lang_pos, m_index.lang_len);
}

string_view locale::
country() const noexcept
{
    return str::substr(m_name, m_index.country_pos, m_index.country_len);
}

string_view locale::
encoding() const noexcept
{
    return str::substr(m_name, m_index.encoding_pos, m_index.encoding_len);
}

string_view locale::
modifier() const noexcept
{
    return str::substr(m_name, m_index.modifier_pos, m_index.modifier_len);
}

std::ostream&
operator<<(std::ostream& os, locale const& l)
{
    return os << l.m_name;
}

} // namespace stream9::xdg::desktop_entry_
