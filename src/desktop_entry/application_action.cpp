#include <stream9/xdg/desktop_entry/application_action.hpp>

#include "desktop_entry_p.hpp"

#include <stream9/xdg/desktop_entry/entry.hpp>

#include <cassert>

namespace stream9::xdg::desktop_entry_ {

application_action::
application_action(entry_group const& g) noexcept
    : m_group { &g }
{
    assert(is_desktop_action_group(*m_group));
}

string_view application_action::
key_name() const noexcept
{
    auto result = get_desktop_action_name(*m_group);

    assert(!result.empty());
    return result;
}

string application_action::
name(locale const& loc/*= {}*/) const
{
    try {
        return get_locale_string_value(*m_group, "Name", loc);
    }
    catch (...) {
        rethrow_error();
    }
}

string application_action::
icon(locale const& loc/*= {}*/) const
{
    try {
        return get_locale_string_value(*m_group, "Icon", loc);
    }
    catch (...) {
        rethrow_error();
    }
}

string application_action::
exec() const
{
    try {
        return get_string_value(*m_group, "Exec");
    }
    catch (...) {
        rethrow_error();
    }
}

bool application_action::
contains(key const& key) const noexcept
{
    return m_group->contains(key);
}

string application_action::
value(key const& key) const
{
    try {
        auto const it = m_group->find(key);
        if (it == m_group->end()) {
            return {};
        }

        return unescape(it->value);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::desktop_entry_
