#ifndef STREAM9_XDG_DESKTOP_ENTRY_DESKTOP_ENTRY_P_HPP
#define STREAM9_XDG_DESKTOP_ENTRY_DESKTOP_ENTRY_P_HPP

#include "namespace.hpp"

#include <stream9/xdg/desktop_entry/entry.hpp>
#include <stream9/xdg/desktop_entry/escape_sequence.hpp>
#include <stream9/xdg/desktop_entry/key.hpp>
#include <stream9/xdg/desktop_entry/locale.hpp>

#include <stream9/array.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::desktop_entry_ {

locale current_locale() noexcept;

array<string_view, 10>
    split_string_list(string_view list);

string get_string_value(entry_group const&, string_view key);

string get_locale_string_value(
              entry_group const&, string_view name, locale const&);

bool get_boolean_value(entry_group const&, string_view key) noexcept;

opt<bool> get_tribool_value(entry_group const&, string_view key) noexcept;

bool is_desktop_action_group(entry_group const&) noexcept;
string_view get_desktop_action_name(entry_group const&) noexcept;

template<typename Callback>
void
get_strings_value(entry_group const& group,
                  string_view key, Callback const& cb)
{
    try {
        auto it = group.find(key);
        if (it != group.end()) {
            for (auto const& element: split_string_list(it->value)) {
                cb(unescape(element));
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Callback>
void
get_locale_strings_value(entry_group const& group,
                         string_view name, locale const& loc,
                         Callback const& cb)
{
    try {
        key k { name, loc.empty() ? current_locale() : loc };

        auto it = group.find(k);
        if (it != group.end()) {
            for (auto const& element: split_string_list(it->value)) {
                cb(unescape(element));
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

inline constexpr string_view
desktop_action_group_prefix() noexcept
{
    return "Desktop Action ";
}

} // namespace stream9::xdg::desktop_entry_

#endif // STREAM9_XDG_DESKTOP_ENTRY_DESKTOP_ENTRY_P_HPP
