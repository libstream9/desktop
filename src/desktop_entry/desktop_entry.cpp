#include <stream9/xdg/desktop_entry.hpp>

#include "desktop_entry_p.hpp"
#include "namespace.hpp"
#include "parser/desktop_entry.hpp"
#include "parser/exec_string.hpp"

#include <stream9/xdg/environment.hpp>
#include <stream9/xdg/desktop_entry/escape_sequence.hpp>

#include <cassert>
#include <clocale>
#include <utility>

#include <stream9/contains.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/less.hpp>
#include <stream9/push_back.hpp>
#include <stream9/ranges/binary_find.hpp>
#include <stream9/ranges/contains.hpp>
#include <stream9/ranges/iterator_at.hpp>
#include <stream9/strings/find_first.hpp>
#include <stream9/strings/replace.hpp>
#include <stream9/strings/starts_with.hpp>
#include <stream9/strings/substr.hpp>
#include <stream9/strings/split.hpp>

namespace stream9::xdg {

namespace desktop_entry_ {

    static bool
    to_bool(string_view v)
    {
        return v == "true";
    }

    locale
    current_locale() noexcept
    {
        return { std::setlocale(LC_MESSAGES, nullptr) };
    }

    array<string_view, 10>
    split_string_list(string_view list)
    {
        try {
            array<string_view, 10> result;
            constexpr char separator = ';';
            constexpr char escape = '\\';

            auto it = list.begin(), end = list.end();
            auto mark = it;

            for (; it != end; ++it) {
                if (*it == separator) {
                    if (it == list.begin() || *(it - 1) != escape) {
                        if (it != mark) { // skip empty
                            st9::emplace_back(result, mark, it);
                        }
                        mark = it + 1;
                    }
                }
            }

            if (it != mark) { // skip empty
                st9::emplace_back(result, mark, it);
            }

            return result;
        }
        catch (...) {
            rethrow_error();
        }
    }

    string
    get_string_value(entry_group const& group, string_view key)
    {
        try {
            auto it = group.find(key);
            if (it == group.end()) {
                return {};
            }

            return unescape(it->value);
        }
        catch (...) {
            rethrow_error();
        }
    }

    string
    get_locale_string_value(entry_group const& group,
                            string_view name, locale const& loc)
    {
        try {
            key k { name, loc.empty() ? current_locale() : loc };

            auto it = group.find(k);
            if (it == group.end()) {
                return {};
            }

            return unescape(it->value);
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool
    get_boolean_value(entry_group const& group, string_view key) noexcept
    {
        auto it = group.find(key);
        if (it == group.end()) {
            return false;
        }

        return to_bool(it->value);
    }

    opt<bool>
    get_tribool_value(entry_group const& group, string_view key) noexcept
    {
        opt<bool> result;

        auto it = group.find(key);
        if (it != group.end()) {
            result = to_bool(it->value);
        }

        return result;
    }

    bool
    is_desktop_action_group(entry_group const& g) noexcept
    {
        auto constexpr prefix = desktop_action_group_prefix();

        return str::starts_with(g.name(), prefix);
    }

    string_view
    get_desktop_action_name(entry_group const& g) noexcept
    {
        auto constexpr prefix = desktop_action_group_prefix();

        if (is_desktop_action_group(g)) {
            return str::substr(g.name(), static_cast<ptrdiff_t>(prefix.size()));
        }
        else {
            return "";
        }
    }

} // namespace desktop_entry_

using entry_group = desktop_entry_::entry_group;
using entry_group_range = desktop_entry::entry_group_range;

static entry_group const&
get_desktop_entry_group(array<entry_group, 1> const& groups) noexcept
{
    return groups.front();
}

/*
 * desktop_entry::impl
 */
class desktop_entry::impl
{
public:
    impl(string_view id, string_view path)
        try : m_id { id }
            , m_text { env().load_file(path) }
            , m_origin { path }
    {
        parser::desktop_entry::parse(m_text, m_entry_groups);
    }
    catch (...) {
        rethrow_error();
    }

    string_view id() const noexcept { return m_id; }

    auto& text() noexcept { return m_text; }
    auto const& text() const noexcept { return m_text; }

    auto const& origin() const noexcept { return m_origin; }

    auto& entry_groups() noexcept { return m_entry_groups; }
    auto const& entry_groups() const noexcept { return m_entry_groups; }

private:
    string m_id;
    string m_text; // content of desktop entry file.
    string m_origin;
    entry_group_set m_entry_groups; // sorted by name
};

/*
 * desktop_entry
 */
desktop_entry::
desktop_entry(string_view id, string_view path) // parser::error
    try : m_p { id, path }
{}
catch (...) {
    rethrow_error({
        { "id", id },
        { "path", path }
    });
}

desktop_entry::desktop_entry(desktop_entry&&) noexcept = default;
desktop_entry& desktop_entry::operator=(desktop_entry&&) noexcept = default;

desktop_entry::~desktop_entry() noexcept = default;

void
swap(desktop_entry& lhs, desktop_entry& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_p, rhs.m_p);
}

string_view desktop_entry::
id() const noexcept
{
    return m_p->id();
}

string desktop_entry::
type() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_string_value(group, "Type");
    }
    catch (...) {
        rethrow_error();
    }
}

string desktop_entry::
name(locale const& loc/*= {}*/) const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_locale_string_value(group, "Name", loc);
    }
    catch (...) {
        rethrow_error();
    }
}

string desktop_entry::
version() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_string_value(group, "Version");
    }
    catch (...) {
        rethrow_error();
    }
}

string desktop_entry::
generic_name(locale const& loc/*= {}*/) const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_locale_string_value(group, "GenericName", loc);
    }
    catch (...) {
        rethrow_error();
    }
}

bool desktop_entry::
no_display() const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return get_boolean_value(group, "NoDisplay");
}

string desktop_entry::
comment(locale const& loc/*= {}*/) const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_locale_string_value(group, "Comment", loc);
    }
    catch (...) {
        rethrow_error();
    }
}

string desktop_entry::
icon(locale const& loc/*= {}*/) const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_locale_string_value(group, "Icon", loc);
    }
    catch (...) {
        rethrow_error();
    }
}

bool desktop_entry::
hidden() const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return get_boolean_value(group, "Hidden");
}

desktop_entry::string_set<5> desktop_entry::
only_show_in() const
{
    try {
        desktop_entry::string_set<5> result;

        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        get_strings_value(group, "OnlyShowIn",
            [&](auto&& v) {
                result.insert(std::move(v));
            });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

desktop_entry::string_set<5> desktop_entry::
not_show_in() const
{
    try {
        desktop_entry::string_set<5> result;

        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        get_strings_value(group, "NotShowIn",
            [&](auto&& v) {
                result.insert(std::move(v));
            });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

bool desktop_entry::
should_show_in_current_desktop() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        bool default_action = !group.contains("OnlyShowIn");

        string_view env = std::getenv("XDG_CURRENT_DESKTOP");

        if (!env.empty()) {
            auto white_list = only_show_in();
            auto black_list = not_show_in();
            for (auto const& value: str::split(env, ";")) {
                if (rng::contains(white_list, string_view(value))) {
                    return true;
                }
                else if (rng::contains(black_list, value)) {
                    return false;
                }
            }
        }

        return default_action;
    }
    catch (...) {
        rethrow_error();
    }
}

bool desktop_entry::
dbus_activatable() const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return get_boolean_value(group, "DBusActivatable");
}

string desktop_entry::
try_exec() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_string_value(group, "TryExec");
    }
    catch (...) {
        rethrow_error();
    }
}

desktop_entry::string_list<5> desktop_entry::
exec() const
{
    try {
        auto exec = exec_raw();

        for (auto i = exec.begin(); i != exec.end();) {
            auto& arg = *i;

            auto [i2, _] = str::find_first(arg, '%');
            if (i2 == arg.end()) {
                ++i;
                continue;
            }

            auto i3 = i2 + 1;
            if (i3 == arg.end()) {
                ++i;
                continue;
            }

            switch (*i3) {
                case 'f':
                case 'u':
                case 'F':
                case 'U':
                    i = exec.erase(i);
                    break;
                case 'i':
                    str::replace(arg, str::string_range(i2, i3 + 1), icon());
                    ++i;
                    break;
                case 'c':
                    str::replace(arg, str::string_range(i2, i3 + 1), name());
                    ++i;
                    break;
                case 'k':
                    str::replace(arg, str::string_range(i2, i3 + 1), origin());
                    ++i;
                    break;
                case 'd': // deprecated codes
                case 'D':
                case 'n':
                case 'N':
                case 'v':
                case 'm':
                    i = exec.erase(i);
                    break;
                default:
                    ++i;
                    break;
            }
        }

        return exec;
    }
    catch (...) {
        rethrow_error();
    }
}

desktop_entry::string_list<5> desktop_entry::
exec(string_view filepath) const
{
    try {
        auto exec = exec_raw();

        bool expanded = false;
        for (auto i = exec.begin(); i != exec.end();) {
            auto& arg = *i;

            auto [i2, _] = str::find_first(arg, '%');
            if (i2 == arg.end()) {
                ++i;
                continue;
            }

            auto i3 = i2 + 1;
            if (i3 == arg.end()) {
                ++i;
                continue;
            }

            switch (*i3) {
                case 'f':
                case 'u':
                    if (!expanded) {
                        str::replace(arg, str::string_range(i2, i3 + 1), filepath); // ignore invalid case that is there are two or more %f, %u.
                        expanded = true;
                    }
                    else {
                        i = exec.erase(i);
                    }
                    break;
                case 'F':
                case 'U':
                    if (!expanded) {
                        arg = filepath;
                        expanded = true;
                    }
                    else {
                        i = exec.erase(i);
                    }
                    break;
                case 'i':
                    str::replace(arg, str::string_range(i2, i3 + 1), icon());
                    ++i;
                    break;
                case 'c':
                    str::replace(arg, str::string_range(i2, i3 + 1), name());
                    ++i;
                    break;
                case 'k':
                    str::replace(arg, str::string_range(i2, i3 + 1), origin());
                    ++i;
                    break;
                case 'd': // deprecated codes
                case 'D':
                case 'n':
                case 'N':
                case 'v':
                case 'm':
                    i = exec.erase(i);
                    break;
                default:
                    ++i;
                    break;
            }
        }

        return exec;
    }
    catch (...) {
        rethrow_error({
            { "filepath", filepath }
        });
    }
}

desktop_entry::string_list<5> desktop_entry::
exec_raw() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());
        auto const v = get_string_value(group, "Exec");

        return parser::exec_string::parse(v);
    }
    catch (...) {
        rethrow_error();
    }
}

string desktop_entry::
path() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_string_value(group, "Path");
    }
    catch (...) {
        rethrow_error();
    }
}

bool desktop_entry::
terminal() const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return get_boolean_value(group, "Terminal");
}

desktop_entry::string_list<5> desktop_entry::
mime_types() const
{
    try {
        string_list<5> result;
        string_set<5> index;

        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        get_strings_value(group, "MimeType",
            [&](auto s) {
                if (auto [_, ok] = index.insert(s); ok) {
                    st9::push_back(result, std::move(s));
                }
            });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

desktop_entry::string_set<5> desktop_entry::
categories() const
{
    try {
        desktop_entry::string_set<5> result;

        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        get_strings_value(group, "Categories",
            [&](auto s) {
                result.insert(std::move(s));
            });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

desktop_entry::string_set<5> desktop_entry::
implements() const
{
    try {
        desktop_entry::string_set<5> result;

        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        get_strings_value(group, "Implements",
            [&](auto s) {
                result.insert(std::move(s));
            });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

desktop_entry::string_set<5> desktop_entry::
keywords(locale const& loc/*= {}*/) const
{
    try {
        desktop_entry::string_set<5> result;

        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        get_locale_strings_value(group, "Keywords", loc,
            [&](auto s) {
                result.insert(std::move(s));
            });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<bool> desktop_entry::
startup_notify() const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return get_tribool_value(group, "StartupNotify");
}

string desktop_entry::
startup_wm_class() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_string_value(group, "StartupWMClass");
    }
    catch (...) {
        rethrow_error();
    }
}

string desktop_entry::
url() const
{
    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        return get_string_value(group, "URL");
    }
    catch (...) {
        rethrow_error();
    }
}

bool desktop_entry::
prefer_non_default_gpu() const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return get_boolean_value(group, "PreferNonDefaultGPU");
}

array<class desktop_entry_::application_action, 5> desktop_entry::
application_actions() const
{
    using namespace desktop_entry_;

    try {
        array<class application_action, 5> result;

        assert(!m_p->entry_groups().empty());

        auto constexpr prefix = desktop_action_group_prefix();

        // skip desktop entry group
        auto begin = rng::iterator_at(m_p->entry_groups(), 1);
        auto end = m_p->entry_groups().end();

        auto groups = std::ranges::equal_range(
            begin, end,
            prefix,
            less(),
            [&](auto&& e) { return str::substr(e.name(), 0, prefix.size()); }
        );

        for (auto const& group: groups) {
            st9::push_back(result, group);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<class desktop_entry_::application_action> desktop_entry::
application_action(string_view name) const noexcept
{
    using namespace desktop_entry_;

    assert(!m_p->entry_groups().empty());

    opt<class application_action> result;

    // skip desktop entry group
    auto begin = rng::iterator_at(m_p->entry_groups(), 1);
    auto end = m_p->entry_groups().end();

    auto it =
        rng::binary_find(begin, end, name, {}, get_desktop_action_name);
    if (it != end) {
        result = *it;
    }

    return result;
}

bool desktop_entry::
contains(key const& key) const noexcept
{
    auto const& group = get_desktop_entry_group(m_p->entry_groups());

    return group.contains(key);
}

string desktop_entry::
value(key const& key) const
{
    using namespace desktop_entry_;

    try {
        auto const& group = get_desktop_entry_group(m_p->entry_groups());

        auto it = group.find(key);
        if (it == group.end()) {
            return {};
        }

        return unescape(it->value);
    }
    catch (...) {
        rethrow_error();
    }
}

entry_group_range desktop_entry::
groups() const noexcept
{
    return m_p->entry_groups();
}

desktop_entry_::entry_group const* desktop_entry::
group(string_view name) const noexcept
{
    assert(!m_p->entry_groups().empty());

    auto it = std::ranges::lower_bound(m_p->entry_groups(), name, {},
        [](auto&& g) { return g.name(); } );

    if (it != m_p->entry_groups().end() && it->name() == name) {
        return &*it;
    }
    else {
        return nullptr;
    }
}

string_view desktop_entry::
text() const noexcept
{
    return m_p->text();
}

string_view desktop_entry::
origin() const noexcept
{
    return m_p->origin();
}

std::ostream&
operator<<(std::ostream& o, desktop_entry const& e)
{
    return o << e.text();
}

} // namespace stream9::xdg
