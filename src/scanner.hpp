#ifndef STREAM9_DESKTOP_SCANNER_HPP
#define STREAM9_DESKTOP_SCANNER_HPP

namespace stream9::xdg::scanner {

enum class next_action {
    proceed = 0,
    stop,
    skip_line,
    skip_group,
};

class stop {};
class skip_line {};
class skip_group {};

inline void
execute_action(next_action const a)
{
    switch (a) {
        case next_action::stop:
            throw stop();
        case next_action::skip_line:
            throw skip_line();
        case next_action::skip_group:
            throw skip_group();
        default:
            break;
    }
}

} // namespace stream9::xdg::scanner

#endif // STREAM9_DESKTOP_SCANNER_HPP
