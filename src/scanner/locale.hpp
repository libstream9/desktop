#ifndef STREAM9_DESKTOP_SCANNER_LOCALE_HPP
#define STREAM9_DESKTOP_SCANNER_LOCALE_HPP

#include "../scanner.hpp"

#include <stream9/string_view.hpp>

#include <system_error>

namespace stream9::xdg::scanner::locale {

// Grammar
//
// locale := lang ?('_' country) ?('.' encoding) ?('@' modifier)
// lang := identifier
// country := identifier
// encoding := identifier
// modifier := identifier
// identifier := +[A-Za-z0-9-]

using iterator = string_view::iterator;

template<typename T>
concept content_handler =
    requires (T&& t, string_view s, std::error_code e, next_action& a)
    {
        t.on_locale(s, a);
        t.on_lang(s, a);
        t.on_country(s, a);
        t.on_encoding(s, a);
        t.on_modifier(s, a);
        t.on_error(e, s, a);
    };

template<content_handler T>
bool scan(string_view text, T&);

template<content_handler T, typename IsEnd>
bool scan(iterator& it, iterator end, T&, IsEnd);

enum class errc {
    ok = 0,
    invalid_char,
    empty_lang,
    empty_country,
    empty_encoding,
    empty_modifier,
};

// Implementation

template<content_handler T>
struct context
{
    T& handler;

    context(T& h)
        : handler { h }
    {}
};

std::error_category const& error_category() noexcept;

inline bool
is_end_default(iterator it, iterator end) noexcept
{
    return it == end;
}

inline std::error_code
make_error_code(errc c) noexcept
{
    return std::error_code(
        static_cast<int>(c),
        error_category()
    );
}

template<content_handler T>
void
emit_error(context<T>& cxt, errc e, iterator it)
{
    next_action a = next_action::stop;
    cxt.handler.on_error(make_error_code(e), { it, it + 1 }, a);
    execute_action(a);
}

template<content_handler T>
void
emit_error(context<T>& cxt, errc e, string_view s)
{
    next_action a = next_action::stop;
    cxt.handler.on_error(make_error_code(e), s, a);
    execute_action(a);
}

template<content_handler T>
void
emit_lang(context<T>& cxt, string_view s)
{
    next_action a = next_action::proceed;
    cxt.handler.on_lang(s, a);
    execute_action(a);
}

template<content_handler T>
void
emit_country(context<T>& cxt, string_view s)
{
    next_action a = next_action::proceed;
    cxt.handler.on_country(s, a);
    execute_action(a);
}

template<content_handler T>
void
emit_encoding(context<T>& cxt, string_view s)
{
    next_action a = next_action::proceed;
    cxt.handler.on_encoding(s, a);
    execute_action(a);
}

template<content_handler T>
void
emit_modifier(context<T>& cxt, string_view s)
{
    next_action a = next_action::proceed;
    cxt.handler.on_modifier(s, a);
    execute_action(a);
}

template<content_handler T>
void
emit_locale(context<T>& cxt, string_view s)
{
    next_action a = next_action::proceed;
    cxt.handler.on_locale(s, a);
    execute_action(a);
}

static bool
is_identifier_char(auto c) noexcept
{
    return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')
        || ('0' <= c && c <= '9') || c == '-';
}

template<content_handler T, typename IsEnd>
void
lang(iterator& it, iterator end, context<T>& cxt, IsEnd is_end)
{
    auto save = it;

    for (; !is_end(it, end); ++it) {
        if (*it == '_' || *it == '.' || *it == '@') break;
        if (!is_identifier_char(*it)) {
            emit_error(cxt, errc::invalid_char, { it, it + 1 });
        }
    }

    if (it == save) {
        emit_error(cxt, errc::empty_lang, { save, it });
    }

    emit_lang(cxt, { save, it });
}

template<content_handler T, typename IsEnd>
void
country(iterator& it, iterator end, context<T>& cxt, IsEnd is_end)
{
    auto save = it;

    for (; !is_end(it, end); ++it) {
        if (*it == '.' || *it == '@') break;
        if (!is_identifier_char(*it)) {
            emit_error(cxt, errc::invalid_char, { it, it + 1 });
        }
    }

    if (it == save) {
        emit_error(cxt, errc::empty_country, { save, it });
    }

    emit_country(cxt, { save, it });
}

template<content_handler T, typename IsEnd>
void
encoding(iterator& it, iterator end, context<T>& cxt, IsEnd is_end)
{
    auto save = it;

    for (; !is_end(it, end); ++it) {
        if (*it == '@') break;
        if (!is_identifier_char(*it)) {
            emit_error(cxt, errc::invalid_char, { it, it + 1 });
        }
    }

    if (it == save) {
        emit_error(cxt, errc::empty_encoding, { save, it });
    }

    emit_encoding(cxt, { save, it });
}

template<content_handler T, typename IsEnd>
void
modifier(iterator& it, iterator end, context<T>& cxt, IsEnd is_end)
{
    auto save = it;

    for (; !is_end(it, end); ++it) {
        if (!is_identifier_char(*it)) {
            emit_error(cxt, errc::invalid_char, { it, it + 1 });
        }
    }

    if (it == save) {
        emit_error(cxt, errc::empty_modifier, { it, save });
    }

    emit_modifier(cxt, { save, it });
}

template<content_handler T, typename IsEnd>
void
locale(iterator& it, iterator end, context<T>& cxt, IsEnd is_end)
{
    auto save = it;

    lang(it, end, cxt, is_end);

    if (it != end && *it == '_') {
        ++it;
        country(it, end, cxt, is_end);
    }

    if (it != end && *it == '.') {
        ++it;
        encoding(it, end, cxt, is_end);
    }

    if (it != end && *it == '@') {
        ++it;
        modifier(it, end, cxt, is_end);
    }

    emit_locale(cxt, { save, it });
}

template<content_handler T, typename IsEnd>
bool
scan(iterator& it, iterator end, T& h, IsEnd is_end)
{
    try {
        context<T> cxt { h };

        locale(it, end, cxt, is_end);
    }
    catch (stop const&) {
        return false;
    }

    return true;
}

template<content_handler T>
bool
scan(string_view text, T& h)
{
    auto it = text.begin();
    return scan(it, text.end(), h, is_end_default);
}

} // namespace stream9::xdg::scanner::locale

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::scanner::locale::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_DESKTOP_SCANNER_LOCALE_HPP
