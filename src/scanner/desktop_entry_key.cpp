#include "desktop_entry_key.hpp"

#include <system_error>

namespace stream9::xdg::scanner::desktop_entry_key {

class error_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "scanner::desktop_entry_key";
    }

    std::string message(int const ev) const override
    {
        switch (static_cast<errc>(ev)) {
            case errc::ok:
                return "ok";
            case errc::invalid_char:
                return "invalid character";
            case errc::empty_name:
                return "empty name";
            case errc::terminator_is_expected:
                return "terminator is expected";
            case errc::end_of_string_is_expected:
                return "end of string is expected";
            default:
                return "unknown error";
        }
    }
};

std::error_category const&
error_category()
{
    static error_category_impl impl;
    return impl;
}

} // namespace stream9::xdg::scanner::desktop_entry_key
