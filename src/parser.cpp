#include "parser.hpp"

#include "namespace.hpp"

#include <algorithm>
#include <ostream>

#include <stream9/push_back.hpp>
#include <stream9/ranges/index_at.hpp>
#include <stream9/ranges/last_index.hpp>

namespace stream9::xdg::parser {

std::ostream&
operator<<(std::ostream& os, location const& loc)
{
    os << "[" << loc.line << ", " << loc.col << "]";
    return os;
}

/*
 * location_tracker
 */
void location_tracker::
new_line(iterator pos)
{
    try {
        st9::push_back(m_bols, pos);
    }
    catch (...) {
        rethrow_error();
    }
}

location location_tracker::
get_location(iterator pos) const noexcept
{
    assert(!m_bols.empty());
    assert(pos != iterator());

    location loc;

    auto it = std::ranges::lower_bound(m_bols, pos);

    if (it == m_bols.end()) {
        auto bol = m_bols.back();
        assert(bol <= pos);

        loc.line = rng::last_index(m_bols) + 1;
        loc.col = pos - bol;
    }
    else {
        if (*it > pos) {
            --it;
        }

        auto bol = *it;
        assert(bol <= pos);

        loc.line = rng::index_at(m_bols, it) + 1;
        loc.col = pos - bol;
    }

    return loc;
}

} // namespace stream9::xdg::parser
