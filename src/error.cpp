#include <stream9/xdg/error.hpp>

namespace stream9::xdg {

std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept { return "stream9::xdg"; }

        std::string message(int ec) const
        {
            switch (static_cast<errc>(ec)) {
                using enum errc;
                case ok:
                    return "ok";
                case parse_error:
                    return "parse error";
                case association_error:
                    return "association error";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::xdg
