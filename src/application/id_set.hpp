#ifndef STREAM9_XDG_APPLICATION_APPLICATION_ID_SET_HPP
#define STREAM9_XDG_APPLICATION_APPLICATION_ID_SET_HPP

#include <stream9/array.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::applications {

using id_set = array<string, 1>;

} // namespace stream9::xdg::applications

#endif // STREAM9_XDG_APPLICATION_APPLICATION_ID_SET_HPP
