#ifndef STREAM9_XDG_APPLICATION_MIME_INFO_CACHE_HPP
#define STREAM9_XDG_APPLICATION_MIME_INFO_CACHE_HPP

#include <stream9/xdg/error.hpp>

#include "time_stamp.hpp"

#include "../namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_table.hpp>

#include <time.h>

namespace stream9::xdg::applications {

class mime_info_cache
{
public:
    using entry_map = unique_table<string_view /*mime_type*/,
                                   string_view /*ids*/ >;
    using desktop_file_id_list = array<string_view, 10>;
    using const_iterator = entry_map::const_iterator;

public:
    mime_info_cache(string_view path) noexcept;

    // accessor
    cstring_view path() const noexcept { return m_path; }

    // accessor
    const_iterator begin() const noexcept { return m_entries.begin(); }
    const_iterator end() const noexcept { return m_entries.end(); }

    // query
    auto size() const noexcept { return m_entries.size(); }

    desktop_file_id_list find(string_view mime_type) const;

    // modifier
    void reload();

private:
    string m_path;
    opt<time_stamp> m_mtime;
    string m_text;
    entry_map m_entries;
};

} // namespace stream9::xdg::applications

#endif // STREAM9_XDG_APPLICATION_MIME_INFO_CACHE_HPP
