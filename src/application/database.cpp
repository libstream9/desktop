#include <stream9/xdg/application/database.hpp>

#include "database_p.hpp"

#include <stream9/xdg/desktop_entry.hpp>
#include <stream9/xdg/environment.hpp>

#include "mime_info_cache.hpp"
#include "parser/mime_apps_list.hpp"

#include <stream9/array.hpp>
#include <stream9/contains.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/errors.hpp>
#include <stream9/filesystem/walk_directory.hpp>
#include <stream9/find.hpp>
#include <stream9/log.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/path/normalize.hpp>
#include <stream9/push_back.hpp>
#include <stream9/range.hpp>
#include <stream9/string.hpp>
#include <stream9/strings/empty.hpp>
#include <stream9/strings/ends_with.hpp>
#include <stream9/strings/find_replace.hpp>
#include <stream9/strings/starts_with.hpp>
#include <stream9/strings/substr.hpp>
#include <stream9/strings/split.hpp>
#include <stream9/strings/views/to_lower.hpp>
#include <stream9/unique_array.hpp>
#include <stream9/xdg/base_dir.hpp>
#include <stream9/xdg/mime/database.hpp>

#include <time.h>

namespace stream9::xdg::applications::v2 {

namespace st9 = stream9;
namespace fs = stream9::filesystem;

using path::operator/;

static void
wipe_association_for_id(sorted_table<string, string>& mime_to_ids,
                        string_view id) noexcept
{
    st9::erase_if(mime_to_ids, [&](auto& kv) {
        return kv.value == id;
    });
}

static auto
operator<=>(struct ::timespec const& x, struct ::timespec const& y) noexcept
{
    auto z = x.tv_sec <=> y.tv_sec;
    if (z == 0) {
        return x.tv_nsec <=> y.tv_nsec;
    }
    else {
        return z;
    }
}

static string
desktop_entry_id(string_view basepath, string_view dpath, string_view fname)
{
    try {
        string id;

        if (dpath != basepath) {
            auto relpath = str::substr(dpath, basepath.size() + 1); // basepath + "/"

            id = relpath / fname;
            str::find_replace_all(id, "/", "-");
        }
        else {
            id = fname;
        }

        return id;
    }
    catch (...) {
        rethrow_error();
    }
}

static struct ::timespec
scan_desktop_entries(unique_table<string, string>& id_to_fpath,
                     sorted_table<string, string>& mime_to_ids,
                     string_view dirpath)
{
    struct ::timespec mtim {};

    auto o_st = env().file_status(dirpath);
    if (!o_st) {
        return mtim;
    }
    else {
        mtim = o_st->st_mtim;
    }


    fs::walk_directory(dirpath, [&](auto& d, auto& e) {
        try {
            auto n = lx::name(e);

            auto o_st = env().file_status(d, n);
            if (!o_st) return;
            if (mtim < o_st->st_mtim) {
                mtim = o_st->st_mtim;
            }

            if (str::ends_with(n, ".desktop")) {
                auto fpath = d.path() / n;
                auto id = desktop_entry_id(dirpath, d.path(), n);

                auto [i, ok] = id_to_fpath.emplace(id, fpath);
                if (!ok) {
                    i->value = fpath;
                    wipe_association_for_id(mime_to_ids, id);
                }
            }
        }
        catch (...) {
            print_error(log::err());
        }
    });

    return mtim;
}

static void
load_mimeinfo_cache(string_view dirpath,
                    unique_table<string, string>& id_to_fpath,
                    sorted_table<string, string>& mime_to_ids)
{
    try {
        mime_info_cache c { dirpath / "mimeinfo.cache" };
        for (auto& [mt, ids]: c) {
            auto hint = mime_to_ids.begin(); // start emplacing from lower_bound
            for (auto id: str::split(ids, ";")) {
                auto i = id_to_fpath.find(id);
                if (i == id_to_fpath.end()) {
                    log::dbg() << "unknown id in" << dirpath / "mimeinfo.cache" << ":" << id;
                    continue;
                }

                hint = mime_to_ids.emplace_hint(hint, mt, id);
                ++hint;
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
scan_desktop_entries_for_mime_type(
    sorted_table<string, string>& mime_to_ids,
    string_view dirpath)
{
    fs::walk_directory(dirpath, [&](auto& d, auto& e) {
        auto n = lx::name(e);

        if (str::ends_with(n, ".desktop")) {
            try {
                auto fpath = d.path() / n;
                auto id = desktop_entry_id(dirpath, d.path(), n);

                desktop_entry ent { id, fpath };

                if (ent.type() != "Application") return;

                for (auto& mt: ent.mime_types()) {
                    mime_to_ids.emplace_hint(mime_to_ids.end(), mt, id);
                }
            }
            catch (...) {
                print_error(log::err());
            }
        }
    });
}

static void
scan_mime_app_association(unique_table<string, string>& id_to_fpath,
                          sorted_table<string, string>& mime_to_ids,
                          string_view dirpath,
                          struct ::timespec const& latest_mtime)
{
    try {
        auto o_st = env().file_status(dirpath / "mimeinfo.cache");
        if (o_st) {
            if (o_st->st_mtim >= latest_mtime) {
                load_mimeinfo_cache(dirpath, id_to_fpath, mime_to_ids);
            }
            else {
                log::warn() << dirpath / "mimeinfo.cache isn't up-to-date";
                scan_desktop_entries_for_mime_type(mime_to_ids, dirpath);
            }
        }
        else {
            log::warn() << dirpath / "mimeinfo.cache doesn't exist";
            scan_desktop_entries_for_mime_type(mime_to_ids, dirpath);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
process_added_associations(unique_table<string, id_set> const& added_assocs,
                           string_view dirpath,
                           unique_table<string, string> const& id_to_fpath,
                           sorted_table<string, string>& mime_to_ids)
{
    for (auto& [mt, ids]: added_assocs) {
        auto m2id = mime_to_ids.equal_range(mt);
        range done { m2id.begin(), m2id.begin() };
        auto rest = m2id;

        for (auto id: ids) {
            try {
                if (id_to_fpath.find(id) == id_to_fpath.end()) {
                    log::warn() << "unknown id in" << dirpath / "mimeapps.list" << ":" << id;
                    continue;
                }
                else {
                    auto find_id = [](auto r, auto id) {
                        return st9::find_if(r, [&](auto& e) { return e.value == id; });
                    };

                    if (find_id(done, id) != done.end()) {
                        log::warn() << "duplicate id in" << dirpath / "mimeapps.list,"
                                    << "mime type =" << mt <<", id =" << id;
                        continue;
                    }

                    auto i = find_id(rest, id);
                    if (i == rest.end()) {
                        auto m2id_first = m2id.begin() - mime_to_ids.begin();
                        auto m2id_size = m2id.size();

                        auto j = mime_to_ids.emplace_hint(rest.begin(), mt, id);

                        m2id = { mime_to_ids.begin() + m2id_first,
                                 mime_to_ids.begin() + m2id_first + m2id_size + 1 };
                        done = { m2id.begin(), j + 1 };
                        rest = { j + 1, m2id.end() };
                    }
                    else {
                        auto j = std::rotate(rest.begin(), i, i + 1); // bring i to the front
                        done = { m2id.begin(), j };
                        rest = { j, rest.end() };
                    }
                }
            }
            catch (...) {
                print_error(log::err());
            }
        }
    }
}

static void
process_removed_associations(unique_table<string, id_set> const& removed_assocs,
                             sorted_table<string, string>& mime_to_ids) noexcept
{
    for (auto& [mt, ids]: removed_assocs) {
        auto i = mime_to_ids.lower_bound(mt);
        while (i != mime_to_ids.end() && i->key == mt) {
            auto& id = i->value;
            if (st9::contains(ids, id)) {
                i = mime_to_ids.erase(i);
            }
            else {
                ++i;
            }
        }
    }
}

static void
process_default_applications(unique_table<string, id_set> const& default_apps,
                             sorted_table<string, string>& mime_to_defs)
{
    // transform [mt, [1,2,3]] into [[mt, 1], [mt, 2], [mt, 3]], then prepend to mime_to_defs
    for (auto& [mt, ids]: default_apps) {
        try {
            // insert 'abc' into '123' where '2' is the lower bound
            // 123 -> 1abc23
            auto h = mime_to_defs.lower_bound(mt);
            for (auto& id: ids) {
                h = mime_to_defs.emplace_hint(h, mt, id);
                ++h;
            }
        }
        catch (...) {
            print_error(log::err());
        }
    }
}

static void
process_mimeapps_list(unique_table<string, string>& id_to_fpath,
                      sorted_table<string, string>& mime_to_ids,
                      sorted_table<string, string>& mime_to_defs,
                      string_view dirpath,
                      string_view desktop)
{
    try {
        auto fname = desktop.empty() ? "mimeapps.list"
                                     : desktop | "-mimeapps.list";
        auto fpath = dirpath / fname;
        if (!env().file_exists(fpath)) return;

        auto text = env().load_file(fpath);

        unique_table<string, id_set> added_assocs, removed_assocs, default_apps;
        parser::mime_apps_list::parse(
            text,
            added_assocs,
            removed_assocs,
            default_apps
        );

        process_added_associations(added_assocs, dirpath, id_to_fpath, mime_to_ids);

        process_removed_associations(removed_assocs, mime_to_ids);

        process_default_applications(default_apps, mime_to_defs);
    }
    catch (...) {
        rethrow_error();
    }
}

static void
process_mimeapps_list(unique_table<string, string>& id_to_fpath,
                      sorted_table<string, string>& mime_to_ids,
                      sorted_table<string, string>& mime_to_defs,
                      string_view dirpath)
{
    auto* env = ::getenv("XDG_CURRENT_DESKTOP");
    if (env) {
        for (auto desktop: str::split(env, ":")) {
            if (str::empty(desktop)) continue;

            string lower_desktop = str::views::to_lower(desktop);

            process_mimeapps_list(
                id_to_fpath, mime_to_ids, mime_to_defs, dirpath, lower_desktop);
        }
    }

    process_mimeapps_list(
        id_to_fpath, mime_to_ids, mime_to_defs, dirpath, "");
}

static void
init_database(unique_table<string, string>& id_to_fpath,
              sorted_table<string, string>& mime_to_ids)
{
    using std::ranges::views::reverse;

    sorted_table<string, string> mime_to_defs;

    for (auto& dirpath: reverse(data_directories())) {
        try {
            if (!env().file_exists(dirpath)) continue;

            auto mtim = scan_desktop_entries(id_to_fpath, mime_to_ids, dirpath);

            scan_mime_app_association(id_to_fpath, mime_to_ids, dirpath, mtim);

            process_mimeapps_list(id_to_fpath, mime_to_ids, mime_to_defs, dirpath);
        }
        catch (...) {
            print_error(log::err());
        }
    }

    for (auto& dirpath: reverse(config_directories())) {
        try {
            if (!env().file_exists(dirpath)) continue;

            process_mimeapps_list(id_to_fpath, mime_to_ids, mime_to_defs, dirpath);
        }
        catch (...) {
            print_error(log::err());
        }
    }

    for (auto& [mt, id]: mime_to_defs) {
        auto m2id = mime_to_ids.equal_range(mt);
        auto i = st9::find_if(m2id, [&](auto& e) { return e.value == id; });
        if (i != m2id.end()) {
            std::rotate(m2id.begin(), i, i + 1); // bring i to the front
            break; // because the one that match first is the default app for the MIME-type.
        }
        else {
            // try next one
        }
    }
}

static void
get_relevant_mime_types(mime::mime_type const& mime_type,
                        auto& result)
{
    try {
        st9::emplace_back(result, mime_type.name());

        for (auto const& base: mime_type.base_classes()) {
            get_relevant_mime_types(base, result);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static auto
get_relevant_mime_types(string_view mime_type,
                        mime::database const& db)
{
    try {
        array<string> result;

        auto const& o_mt = db.find_mime_type_for_name(mime_type);
        if (o_mt) {
            get_relevant_mime_types(*o_mt, result);
        }
        else {
            st9::emplace_back(result, mime_type);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<string>
data_directories()
{
    try {
        array<string> result;

        st9::push_back(result, path::normalize(xdg::data_home() / "applications"));

        for (auto const& path: xdg::data_dirs()) {
            auto p = path::normalize(path / "applications");
            if (!st9::contains(result, p)) {
                st9::push_back(result, std::move(p));
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<string>
config_directories()
{
    try {
        array<string> result;

        st9::push_back(result, path::normalize(xdg::config_home()));

        for (auto const& path: xdg::config_dirs()) {
            auto p = path::normalize(path);
            if (!st9::contains(result, p)) {
                st9::push_back(result, std::move(p));
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class database::impl
 */
database::impl::
impl(xdg::mime::database const& mdb)
    : m_mime_db { mdb }
{
    try {
        init_database(m_id_to_fpath, m_mime_to_ids);
    }
    catch (...) {
        rethrow_error();
    }
}

array<desktop_entry> database::impl::
find_applications(string_view mime_type) const
{
    try {
        array<desktop_entry> result;

        auto mime_types = get_relevant_mime_types(mime_type, m_mime_db);
        for (auto& mt: mime_types) {
            for (auto& [_, id]: m_mime_to_ids.equal_range(mt)) {
                try {
                    auto i = st9::find_if(result, [&](auto& e) { return e.id() == id; });
                    if (i == result.end()) {
                        auto j = m_id_to_fpath.find(id);
                        if (j != m_id_to_fpath.end()) {
                            auto& fpath = j->value;
                            st9::emplace_back(result, id, fpath);
                        }
                        else {
                            log::dbg() << "unknown id in MIME association table: MIME-type =" << mime_type << ", id = " << id;
                        }
                    }
                }
                catch (...) {
                    print_error(log::err());
                }
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<desktop_entry> database::impl::
find_default_application(string_view mime_type) const
{
    try {
        opt<desktop_entry> result;

        auto mime_types = get_relevant_mime_types(mime_type, m_mime_db);
        for (auto& mt: mime_types) {
            auto i = m_mime_to_ids.find(mt); // highest precedence among associations
            if (i != m_mime_to_ids.end()) {
                auto& id = i->value;
                result = load_desktop_entry(id);
                if (result) break;
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static opt<desktop_entry>
load_desktop_entry_(string_view id, string_view fpath) noexcept
{
    try {
        return desktop_entry(id, fpath);
    }
    catch (...) {
        print_error(log::err());
        return {};
    }
}

opt<desktop_entry> database::impl::
load_desktop_entry(string_view id) const noexcept
{
    opt<desktop_entry> result;

    auto i = m_id_to_fpath.find(id);
    if (i != m_id_to_fpath.end()) {
        auto& fpath = i->value;
        result = load_desktop_entry_(id, fpath);
    }

    return result;
}

array<desktop_entry> database::impl::
all_applications() const
{
    try {
        array<desktop_entry> result;

        for (auto& [id, fpath]: m_id_to_fpath) {
            auto o_ent = load_desktop_entry_(id, fpath);
            if (o_ent) {
                if (o_ent->type() != "Application") continue;

                st9::push_back(result, std::move(*o_ent));
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

unique_array<string> database::impl::
associated_mime_types(string_view prefix) const
{
    try {
        unique_array<string> result;

        for (auto& [mt, _]: m_mime_to_ids) {
            if (str::starts_with(mt, prefix)) {
                result.insert(mt);
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

void database::impl::
refresh()
{
    try {
        unique_table<string, string> id_to_fpath;
        sorted_table<string, string> mime_to_ids;

        init_database(id_to_fpath, mime_to_ids);

        m_id_to_fpath = std::move(id_to_fpath);
        m_mime_to_ids = std::move(mime_to_ids);
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class database
 */
database::
database(xdg::mime::database const& mdb)
    : m_p { mdb }
{}

database::~database() noexcept = default;

array<desktop_entry> database::
find_applications(string_view mime_type) const
{
    return m_p->find_applications(mime_type);
}

opt<desktop_entry> database::
find_default_application(string_view mime_type) const
{
    return m_p->find_default_application(mime_type);
}

opt<desktop_entry> database::
load_desktop_entry(string_view id) const noexcept
{
    return m_p->load_desktop_entry(id);
}

array<desktop_entry> database::
all_applications() const
{
    return m_p->all_applications();
}

unique_array<string> database::
associated_mime_types(string_view prefix/*= ""*/) const
{
    return m_p->associated_mime_types(prefix);
}

void database::
refresh()
{
    return m_p->refresh();
}

} // namespace stream9::xdg::applications::v2

