#ifndef STREAM9_XDG_APPLICATION_APPLICATIONS_TIME_STAMP_HPP
#define STREAM9_XDG_APPLICATION_APPLICATIONS_TIME_STAMP_HPP

#include <time.h>

namespace stream9::xdg::applications {

class time_stamp
{
public:
    time_stamp(::timespec const& v) noexcept
        : m_value { v }
    {}

    auto seconds() const noexcept { return m_value.tv_sec; }
    auto nanoseconds() const noexcept { return m_value.tv_nsec; }

    bool operator==(time_stamp const&) const noexcept = default;

    bool operator==(::timespec const& o) const noexcept
    {
        return m_value.tv_sec == o.tv_sec
            && m_value.tv_nsec == o.tv_nsec;
    }

private:
    ::timespec m_value;
};

} // namespace stream9::xdg::applications

#endif // STREAM9_XDG_APPLICATION_APPLICATIONS_TIME_STAMP_HPP
