#include <stream9/xdg/application/database_updater.hpp>

#include "database_p.hpp"

#include <stream9/bits/predicate.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/expand.hpp>
#include <stream9/filesystem/walk_directory.hpp>
#include <stream9/less.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/log.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/projection.hpp>
#include <stream9/ref.hpp>
#include <stream9/strings/ends_with.hpp>
#include <stream9/unique_array.hpp>

namespace stream9::xdg::applications::v2 {

using path::operator/;

namespace fs = stream9::filesystem;

static constexpr std::uint32_t
watch_mask() noexcept
{
    return static_cast<std::uint32_t>(
        IN_CLOSE_WRITE | IN_MOVED_TO
         | IN_DELETE | IN_MOVED_FROM
         | IN_MOVE_SELF | IN_DELETE_SELF );
}

struct database_updater::impl
{
    struct watch {
        int wd;
        enum { app, config } type;
        string dirpath;
    };

    database::impl& m_db;
    lx::inotify m_inotify;
    unique_array<watch, less, project<&watch::wd>> m_watches;
    array<ref<database_updater::event_handler>> m_handlers;

    impl(database::impl& db)
        try : m_db { db }
            , m_inotify { IN_NONBLOCK }
    {
        for (auto& d: data_directories()) {
            try {
                watch_application_directory(d / "applications");
            }
            catch (...) {
                print_error(log::warn());
            }
        }

        for (auto& d: config_directories()) {
            try {
                watch_config_directory(d);
            }
            catch (...) {
                print_error(log::warn());
            }
        }
    }
    catch (...) {
        rethrow_error();
    }

    ~impl() noexcept
    {
        for (auto& h: m_handlers) {
            h->m_src = nullptr;
        }
    }

    lx::fd_ref monitor_fd() const noexcept { return m_inotify.fd(); }

    opt<int>
    add_watch(string_view path)
    {
        try {
            auto wd = m_inotify.add_watch(path, watch_mask());
            return wd;
        }
        catch (...) {
            using enum lx::errc;

            auto ec = current_error_code();
            if (ec == enoent || ec == eacces) {
                return {};
            }
            else {
                rethrow_error();
            }
        }
    }

    void
    watch_application_directory(string_view dirpath)
    {
        using path::operator/;

        try {
            st9::expand(m_watches, 1);

            auto o_wd = add_watch(dirpath);
            if (!o_wd) return;

            m_watches.insert({
                *o_wd, watch::app, dirpath
            });

            fs::walk_directory(dirpath, [&](auto& d, auto& e) {
                auto n = lx::name(e);

                if (lx::is_directory(e, d.fd())) {
                    watch_application_directory(dirpath / n);
                }
            });
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    watch_config_directory(string_view dirpath)
    {
        try {
            auto o_wd = add_watch(dirpath);
            if (o_wd) {
                m_watches.insert({
                    *o_wd, watch::config, dirpath
                });
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    unwatch_directory(int wd) noexcept
    {
        auto it = m_watches.find(wd);
        m_watches.erase(it);
    }

    bool
    process_application_directory_event(auto& ev)
    {
        using path::operator/;

        try {
            auto n = lx::name(ev);

            if (n == "mimeinfo.cache") {
                return true;
            }
            else if (bits::any_of(ev.mask, IN_DELETE_SELF | IN_MOVE_SELF)) {
                return true;
            }
            else if (str::ends_with(n, ".desktop")) {
                return true;
            }
            else if (str::ends_with(n, "mimeapps.list")) {
                return true;
            }

            return false;
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool
    process_config_directory_event(auto& ev)
    {
        try {
            auto n = lx::name(ev);

            return str::ends_with(n, "mimeapps.list");
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool
    process_inotify_event(auto& ev) noexcept
    {
        try {
            auto it = m_watches.find(ev.wd);
            if (it == m_watches.end()) return false;

            if (bits::any_of(ev.mask, IN_IGNORED)) {
                unwatch_directory(ev.wd);
                return false;
            }

            bool dirty = false;
            if (it->type == watch::app) {
                dirty |= process_application_directory_event(ev);
            }
            else if (it->type == watch::config) {
                dirty |= process_config_directory_event(ev);
            }

            if (dirty) {
                refresh_database();
            }
        }
        catch (...) {
            print_error(log::warn());
        }

        return false;
    }

    void
    refresh_database()
    {
        try {
            database::impl db { m_db.mime_db() };

            using std::swap;
            swap(m_db, db);

            auto& [i1, e1] = db.id_to_fpath();
            auto& [i2, e2] = m_db.id_to_fpath();
            while (i1 != e1 && i2 != e2) {
                if (i1->key == i2->key) {
                    if (i1->value == i2->value) {
                        // no change
                    }
                    else {
                        emit_entry_changed(i2->key, i2->value);
                    }
                    ++i1, ++i2;
                }
                else if (i1->key < i2->key) {
                    emit_entry_deleted(i1->key);
                    ++i1;
                }
                else { // i1->key > i2->key
                    emit_entry_added(i2->key, i2->value);
                    ++i2;
                }
            }

            for (; i1 != e1; ++i1) {
                emit_entry_deleted(i1->key);
            }
            for (; i2 != e2; ++i2) {
                emit_entry_added(i2->key, i2->value);
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    emit_entry_added(string_view id, string_view path) const noexcept
    {
        for (auto& h: m_handlers) {
            try {
                h->desktop_entry_added(id, path);
            }
            catch (...) {
                print_error(log::err());
            }
        }
    }

    void
    emit_entry_changed(string_view id, string_view path) const noexcept
    {
        for (auto& h: m_handlers) {
            try {
                h->desktop_entry_changed(id, path);
            }
            catch (...) {
                print_error(log::err());
            }
        }
    }

    void
    emit_entry_deleted(string_view id) const noexcept
    {
        for (auto& h: m_handlers) {
            try {
                h->desktop_entry_deleted(id);
            }
            catch (...) {
                print_error(log::err());
            }
        }
    }

    int
    process_events()
    {
        try {
            int num_processed = 0;
            char buf[1024];

            while (true) {
                auto events = m_inotify.read(buf);
                if (events.empty()) break;

                for (auto const& ev: events) {
                    if (process_inotify_event(ev)) {
                        ++num_processed;
                    }
                }
            }

            return num_processed;
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    add_event_handler(database_updater::event_handler& h)
    {
        try {
            m_handlers.insert(h);
            h.m_src = this;
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    remove_event_handler(database_updater::event_handler& h) noexcept
    {
        st9::erase_if(m_handlers, [&](auto& e) { return &e == &h; });
    }
};

/*
 * class database_updater
 */
database_updater::
database_updater(database& db)
try : m_p { db.m_p }
{}
catch (...) {
    rethrow_error();
}

database_updater::~database_updater() noexcept = default;

lx::fd_ref database_updater::
monitor_fd() const noexcept
{
    return m_p->monitor_fd();
}

safe_integer<int, 0> database_updater::
process_events()
{
    return m_p->process_events();
}

void database_updater::
add_event_handler(event_handler& h)
{
    return m_p->add_event_handler(h);
}

database_updater::event_handler::
~event_handler() noexcept
{
    if (m_src) {
        m_src->remove_event_handler(*this);
    }
}

} // namespace stream9::xdg::applications::v2
