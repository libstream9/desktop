#include "mime_apps_list.hpp"

#include <stream9/xdg/environment.hpp>

#include "namespace.hpp"
#include "parser/mime_apps_list.hpp"

#include <stream9/erase_if.hpp>
#include <stream9/log.hpp>
#include <stream9/push_back.hpp>
#include <stream9/ranges/empty.hpp>
#include <stream9/ranges/find.hpp>
#include <stream9/ranges/lower_bound.hpp>
#include <stream9/ranges/upper_bound.hpp>
#include <stream9/strings/stream.hpp>
#include <stream9/strings/substr.hpp>
#include <stream9/strings/join.hpp>

namespace stream9::xdg::applications {

/*
 * mime_apps_list
 */
mime_apps_list::
mime_apps_list(string_view path) noexcept
    try : m_path { path }
{
    reload();
}
catch (...) {
    print_error(log::warn(), {
        { "path", path }
    });
}

static id_set const&
empty_id_set()
{
    static id_set instance;

    return instance;
}

id_set const& mime_apps_list::
added_associations(string_view mime_type) const
{
    try {
        auto it = m_added_assocs.find(mime_type);
        if (it != m_added_assocs.end()) {
            return it->value;
        }
        else {
            return empty_id_set();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

id_set const& mime_apps_list::
removed_associations(string_view mime_type) const
{
    try {
        auto it = m_removed_assocs.find(mime_type);
        if (it != m_removed_assocs.end()) {
            return it->value;
        }
        else {
            return empty_id_set();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

id_set const& mime_apps_list::
default_applications(string_view const mime_type) const
{
    try {
        auto it = m_default_apps.find(mime_type);
        if (it != m_default_apps.end()) {
            return it->value;
        }
        else {
            return empty_id_set();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
associated_mime_types(string_view prefix,
                      function_ref<void(string_view)> cb) const
{
    try {
        auto get_prefix =
            [&](auto&& e) { return str::substr(e.key, 0, prefix.size()); };

        auto lower = rng::lower_bound(m_added_assocs, prefix, {}, get_prefix);
        auto upper = rng::upper_bound(m_added_assocs, prefix, {}, get_prefix);

        for (auto it = lower; it != upper; ++it) {
            cb(it->key);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

bool mime_apps_list::
empty() const noexcept
{
    return rng::empty(m_added_assocs)
        && rng::empty(m_removed_assocs)
        && rng::empty(m_default_apps);
}

static bool
update_association(auto& assoc_map1,
                   auto& assoc_map2,
                   string_view mime_type, string_view id)
{
    try {
        bool changed = false;

        auto it1 = assoc_map1.find(mime_type);
        if (it1 != assoc_map1.end()) {
            auto& ids = it1->value;
            auto n = st9::erase_if(ids, [&](auto&& i) {
                return i == id;
            });
            if (n != 0) changed = true;

            if (ids.empty()) {
                assoc_map1.erase(it1);
                changed = true;
            }
        }

        auto it2 = assoc_map2.find(mime_type);
        if (it2 != assoc_map2.end()) {
            using std::ranges::find_if;

            auto& ids = it2->value;
            auto it3 = find_if(ids, [&](auto&& i) {
                return i == id;
            });
            if (it3 == ids.end()) {
                st9::push_back(ids, id);
                changed = true;
            }
            else if (it3 != ids.begin()) {
                std::rotate(ids.begin(), it3, it3 + 1); // bring *it3 to the front
                changed = true;
            }
        }
        else {
            assoc_map2.emplace(string(mime_type), id_set { id });
            changed = true;
        }

        return changed;
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
add_association(string_view mime_type, string_view id)
{
    try {
        auto changed =
            update_association(m_removed_assocs, m_added_assocs, mime_type, id);
        if (changed) {
            m_changed = true;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
remove_association(string_view mime_type, string_view id) //TODO test
{
    try {
        auto changed =
            update_association(m_added_assocs, m_removed_assocs, mime_type, id);
        if (changed) {
            m_changed = true;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
add_default_application(string_view mime_type, string_view id)
{
    try {
        auto it = m_default_apps.find(mime_type);
        if (it != m_default_apps.end()) {
            auto& ids = it->value;

            auto it2 = rng::find(ids, id);
            if (it2 == ids.end()) {
                ids.insert(ids.begin(), id); //TODO push_front
                m_changed = true;
            }
            else if (it2 != ids.begin()) {
                std::rotate(ids.begin(), it2, it2 + 1); // bring *it2 to the front
                m_changed = true;
            }
        }
        else {
            m_default_apps.emplace(string(mime_type), id_set { id });
            m_changed = true;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
remove_default_application(string_view mime_type, string_view id)
{
    try {
        auto it1 = m_default_apps.find(mime_type);
        if (it1 != m_default_apps.end()) {
            auto& ids = it1->value;
            auto it2 = rng::find(ids, id);
            ids.erase(it2);

            if (rng::empty(ids)) {
                m_default_apps.erase(it1);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
reload()
{
    try {
        auto o_st = env().file_status(m_path);
        if (o_st) {
            auto& mtime = o_st->st_mtim;
            if (m_mtime && *m_mtime == mtime) return;

            auto text = env().load_file(m_path);
            m_mtime = mtime;

            m_added_assocs.clear();
            m_removed_assocs.clear();
            m_default_apps.clear();

            parser::mime_apps_list::parse(
                text,
                m_added_assocs,
                m_removed_assocs,
                m_default_apps
            );
        }
        else {
            m_added_assocs.clear();
            m_removed_assocs.clear();
            m_default_apps.clear();

            m_mtime = null;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_apps_list::
save()
{
    using str::operator<<;

    if (m_changed) {
        string s;
        s << *this;
        m_mtime = env().save_file(m_path, s);

        m_changed = false;
    }
}

std::ostream&
operator<<(std::ostream& os, mime_apps_list const& x)
{
    try {
        bool first = true;

        if (!rng::empty(x.m_added_assocs)) {
            os << "[Added Associations]\n";
            for (auto& [mt, ids]: x.m_added_assocs) {
                string y = str::join(ids, ";");
                os << mt << "=" << y << "\n";
            }
            first = false;
        }
        if (!rng::empty(x.m_removed_assocs)) {
            if (!first) os << "\n";

            os << "[Removed Associations]\n";
            for (auto& [mt, ids]: x.m_removed_assocs) {
                string y = str::join(ids, ";");
                os << mt << "=" << y << "\n";
            }
            first = false;
        }
        if (!rng::empty(x.m_default_apps)) {
            if (!first) os << "\n";

            os << "[Default Applications]\n";
            for (auto& [mt, ids]: x.m_default_apps) {
                string y = str::join(ids, ";");
                os << mt << "=" << y << "\n";
            }
        }

        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::applications
