#include "directory.hpp"

#include "namespace.hpp"

#include <stream9/append.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/log.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/push_back.hpp>
#include <stream9/strings/ends_with.hpp>
#include <stream9/strings/modifier/append.hpp>
#include <stream9/strings/prefix.hpp>
#include <stream9/strings/substr.hpp>
#include <stream9/strings/view/split.hpp>
#include <stream9/strings/view/to_lower.hpp>
#include <stream9/unique_array.hpp>

#include <algorithm>
#include <cstdlib>

namespace stream9::xdg::applications {

using path::operator/;

static auto
desktop_names()
{
    try {
        array<string, 10> result;
        unique_array<string, less, std::identity, 10> index;

        auto env = std::getenv("XDG_CURRENT_DESKTOP");

        for (auto const& name: str::views::split(env, ";")) {
            string lower = str::views::to_lower(name);

            auto [_, ok] = index.insert(lower);
            if (ok) {
                st9::push_back(result, std::move(lower));
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class directory
 */
directory::
directory(string_view path)
    try : m_path { path }
        , m_mime_info_cache { m_path / "applications/mimeinfo.cache" }
        , m_mime_apps_list { m_path / "mimeapps.list" }
{
    for (auto const& name: desktop_names()) {
        try {
            auto path = m_path / name + "-mimeapps.list";
            class mime_apps_list v { path };
            if (!v.empty()) {
                st9::push_back(m_desktop_mime_apps_lists, std::move(v));
            }
        }
        catch (...) {}
    }
}
catch (...) {
    rethrow_error();
}

directory::desktop_file_id_list directory::
installed_applications(string_view mime_type) const
{
    try {
        return m_mime_info_cache.find(mime_type);
    }
    catch (...) {
        rethrow_error();
    }
}

id_set const& directory::
added_associations(string_view mime_type) const
{
    try {
        return m_mime_apps_list.added_associations(mime_type);
    }
    catch (...) {
        rethrow_error();
    }
}

id_set const& directory::
removed_associations(string_view mime_type) const
{
    try {
        return m_mime_apps_list.removed_associations(mime_type);
    }
    catch (...) {
        rethrow_error();
    }
}

id_set directory::
default_applications(string_view mime_type) const
{
    try {
        id_set result;

        // desktop prefixed mimeapp.list only affect default applications
        for (auto const& list: m_desktop_mime_apps_lists) {
            st9::append(result, list.default_applications(mime_type));
        }

        st9::append(result, m_mime_apps_list.default_applications(mime_type));

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static void
load_application_entries(lx::directory& d, string_view id_prefix, auto cb)
{
    try {
        for (auto& e: d) {
            auto fn = lx::name(e);
            if (fn == "." || fn == "..") continue;

            if (e.d_type == DT_DIR) {
                lx::directory d2 { d.fd(), fn };

                string p = id_prefix + fn + '-';

                load_application_entries(d2, p, cb);
            }
            else {
                auto ext = ".desktop";
                if (!str::ends_with(fn, ext)) continue;

                auto len = str::size(fn) - str::size(ext);
                string id = id_prefix + str::prefix(fn, len);

                try {
                    desktop_entry ent { id, d.path() / fn };
                    if (ent.type() == "Application") {
                        cb(std::move(ent));
                    }
                }
                catch (...) {
                    print_error(log::warn());
                }
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "path", d.path() }
        });
    }
}

static opt<lx::directory>
open_applications_directory(auto path)
{
    opt<lx::directory> d;

    try {
        d = lx::directory(path / "applications");
    }
    catch (...) {
        if (current_error_code() != lx::errc::enoent) {
            rethrow_error();
        }
    }

    return d;
}

void directory::
all_applications(function_ref<void(desktop_entry)> cb) const
{
    try {
        auto o_d = open_applications_directory(m_path);
        if (o_d) {
            load_application_entries(*o_d, "", cb);
        }
    }
    catch (...) {
        rethrow_error({
            { "path", m_path }
        });
    }
}

void directory::
associated_mime_types(string_view prefix,
                      function_ref<void(string_view)> cb) const
{
    try {
        m_mime_apps_list.associated_mime_types(prefix, cb);

        auto get_prefix =
            [&](auto&& e) { return str::substr(e.key, 0, prefix.size()); };
        auto lower = rng::lower_bound(m_mime_info_cache, prefix, {}, get_prefix);
        auto upper = rng::upper_bound(m_mime_info_cache, prefix, {}, get_prefix);

        for (auto it = lower; it != upper; ++it) {
            cb(it->key);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void directory::
reload_mime_info_cache()
{
    try {
        m_mime_info_cache.reload();
    }
    catch (...) {
        rethrow_error();
    }
}

void directory::
reload_mime_apps_list(string_view filename)
{
    try {
        if (filename == "mimeapps.list") {
            m_mime_apps_list.reload();
        }
        else {
            for (auto& list: m_desktop_mime_apps_lists) {
                if (path::basename(list.path()) == filename) {
                    list.reload();
                }
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::applications
