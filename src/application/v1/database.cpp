#include <stream9/xdg/application/v1/database.hpp>

#include "database_p.hpp"
#include "directory.hpp"
#include "namespace.hpp"
#include "parser.hpp"

#include <stream9/xdg/desktop_entry.hpp>
#include <stream9/xdg/environment.hpp>
#include <stream9/xdg/error.hpp>

#include <stream9/array.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/log.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/normalize.hpp>
#include <stream9/push_back.hpp>
#include <stream9/ranges/contains.hpp>
#include <stream9/string_view.hpp>
#include <stream9/strings/ends_with.hpp>
#include <stream9/strings/find_replace.hpp>
#include <stream9/strings/query/contains.hpp>
#include <stream9/unique_array.hpp>
#include <stream9/xdg/base_dir.hpp>
#include <stream9/xdg/mime/database.hpp>

namespace stream9::xdg::applications::v1 {

namespace mime = stream9::xdg::mime;

using directory_range = std::ranges::subrange<directory_set::const_iterator>;

struct association {
    string_view id;
    directory_set::const_iterator dir_it;
};

using preference_ordered_association_list = array<association, 20>;

template<typename Vector, typename Index, typename Value>
static void
insert_unique(Vector& vec, Index& index, Value&& value)
{
    try {
        if (index.insert(value).inserted) {
            st9::push_back(vec, std::forward<Value>(value));
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static auto
get_unique_desktop_directories()
{
    try {
        array<string, 10> result;
        unique_array<string, less, std::identity, 10> index;

        insert_unique(result, index, path::normalize(xdg::config_home()));

        for (auto const& path: xdg::config_dirs()) {
            insert_unique(result, index, path::normalize(path));
        }

        insert_unique(result, index, path::normalize(xdg::data_home()));

        for (auto const& path: xdg::data_dirs()) {
            insert_unique(result, index, path::normalize(path));
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static string
desktop_entry_path(directory_range const& dirs,
                   string_view id)
{
    using path::operator/;

    try {
        for (auto const& dir: dirs) {
            auto const& path = dir.path() / "applications" / id;

            if (env().file_exists(path)) {
                return path;
            }

            if (str::contains(id, "-")) {
                auto name = string(id);
                str::find_replace_all(name, "-", "/");

                auto const& path = dir.path() / "applications" / name;

                if (env().file_exists(path)) {
                    return path;
                }
            }
        }

        return {};
    }
    catch (...) {
        rethrow_error();
    }
}

static opt<desktop_entry>
load_desktop_entry_(directory_range const& dirs, string_view id) noexcept
{
    opt<desktop_entry> result;

    try {
        auto const& path = desktop_entry_path(dirs, id);
        if (!path.empty()) {
            try {
                result.emplace(id, path);
            }
            catch (...) {
                print_error(log::warn(), {
                    { "id", id },
                    { "path", path },
                });
            }
        }
    }
    catch (...) {
        print_error(log::err());
    }

    return result;
}

static opt<desktop_entry>
load_desktop_entry_(directory_range const& dirs,
                    string_view id,
                    database::error_set& errors) noexcept
{
    opt<desktop_entry> result;

    auto const& path = desktop_entry_path(dirs, id);
    if (!path.empty()) {
        try {
            result.emplace(id, path);
        }
        catch (error const& e) {
            auto cxt = e.context();
            cxt["path"] = path;

            try {
                throw_error(errc::parse_error, cxt);
            }
            catch (...) {
                st9::push_back(errors, std::current_exception());
            }
        }
        catch (...) {
            st9::push_back(errors, std::current_exception());
        }
    }

    return result;
}

static void
get_base_mime_types(mime::database const& db,
                    mime::mime_type const& mime_type,
                    auto& result)
{
    try {
        st9::emplace_back(result, mime_type.name());

        for (auto const& base: mime_type.base_classes()) {
            get_base_mime_types(db, base, result);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static auto
get_relevant_mime_types(mime::database const& db,
                        string_view mime_type)
{
    try {
        array<string, 10> result;

        auto const& o_mt = db.find_mime_type_for_name(mime_type);
        if (o_mt) {
            get_base_mime_types(db, *o_mt, result);
        }
        else {
            st9::emplace_back(result, mime_type);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static void
get_associations(directory_set const& dirs,
                 string_view mime_type,
                 preference_ordered_association_list& result)
{
    try {
        using id_set = unique_array<string_view, less, std::identity, 10>;

        id_set black_list;
        id_set white_list;

        for (auto it = dirs.begin(); it != dirs.end(); ++it) {
            auto const& dir = *it;

            for (auto const& id: dir.added_associations(mime_type)) {
                if (!rng::contains(black_list, id)) {
                    if (auto [_, ok] = white_list.insert(id); ok) {
                        st9::emplace_back(result, id, it);
                    }
                }
            }

            for (auto const& id: dir.removed_associations(mime_type)) {
                black_list.insert(id);
            }

            for (auto const& id: dir.installed_applications(mime_type)) {
                if (!rng::contains(black_list, id)) {
                    if (auto [_, ok] = white_list.insert(id); ok) {
                        st9::emplace_back(result, id, it);
                    }
                }
            }

            for (auto const& id: dir.installed_applications(mime_type)) {
                black_list.insert(id);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static preference_ordered_association_list
get_associations_for_relevant_mime_types(mime::database const& db,
                                         directory_set const& dirs,
                                         string_view mime_type)
{
    try {
        preference_ordered_association_list result;

        auto const& mime_types = get_relevant_mime_types(db, mime_type);

        for (auto const& mt: mime_types) {
            get_associations(dirs, mt, result);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static bool
contains(preference_ordered_association_list const& assocs,
         string_view id) noexcept //TODO replace with rng::contains
{
    using std::ranges::find_if;

    auto it = find_if(assocs,
        [&](auto&& a) { return a.id == id; } );

    return it != assocs.end();
}

static auto
current_or_later_directories(directory_set const& dirs,
                             directory_set::const_iterator cur_it) noexcept
{
    return std::ranges::subrange(cur_it, dirs.end());
}

/*
 * database::impl
 */
database::impl::
impl()
{
    try {
        init_directories();
    }
    catch (...) {
        rethrow_error();
    }
}

database::impl::
impl(mime::database const& mime_db)
    : m_external_mime_db { &mime_db }
{
    try {
        init_directories();
    }
    catch (...) {
        rethrow_error();
    }
}

static opt<lx::directory>
open_directory(string_view path) noexcept
{
    try {
        lx::directory d { path };
        return d;
    }
    catch (...) {
        return {};
    }
}

static void
scan_entries(auto& entries, string_view dirpath, string_view id_prefix = "")
{
    using path::operator/;

    try {
        auto o_dir = open_directory(dirpath);
        if (o_dir) {
            for (auto& ent: *o_dir) { //TODO proper directory walker
                auto n = lx::name(ent);
                if (n.empty()) continue;
                if (n[0] == '.') continue;

                if (lx::is_directory(ent, o_dir->fd())) {
                    string p = id_prefix + n + '-';
                    scan_entries(entries, dirpath / n, p);
                }

                if (str::ends_with(n, ".desktop")) {
                    string id = id_prefix + n;
                    entries.emplace(id, dirpath); // can fail if id is already registered.
                }
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void database::impl::
init_directories()
{
    using path::operator/;

    try {
        auto const& dirs = get_unique_desktop_directories();
        m_directories.reserve(dirs.size());

        for (auto const& path: dirs) {
            try {
                if (env().directory_exists(path)) {
                    st9::emplace_back(m_directories, path);

                    scan_entries(m_entries, path / "applications", "");
                }
            }
            catch (...) {
                print_error(log::err());
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

database::preference_ordered_desktop_entry_set database::impl::
find_applications(string_view mime_type) const
{
    try {
        preference_ordered_desktop_entry_set result;
        unique_array<string_view, less, std::identity, 10> id_index;

        auto const& assocs = get_associations_for_relevant_mime_types(
                                             mime_db(), m_directories, mime_type);

        for (auto const& [id, dir_it]: assocs) {
            if (rng::contains(id_index, id)) continue;

            auto o_ent = load_desktop_entry_(
                current_or_later_directories(m_directories, dir_it), id); //TODO this seems wrong

            if (o_ent) {
                st9::push_back(result, std::move(*o_ent));
                id_index.insert(id);
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static error
make_association_error(auto& mime_type, auto& dir, auto& id)
{
    using path::operator/;

    string detail;
    detail << id << " is associated as default application in "
           << (dir.path() / "mimeapps.list") << " but it isn't "
           << "associated in mimeinfo.cache in any directory.";

    return {
        errc::association_error, {
            { "mime_type", mime_type },
            { "description", detail }
        }
    };
}

opt<desktop_entry> database::impl::
find_default_application(string_view mime_type) const
{
    try {
        opt<desktop_entry> result;

        auto const& assocs = get_associations_for_relevant_mime_types(
                                             mime_db(), m_directories, mime_type);
        if (assocs.empty()) {
            return {};
        }

        for (auto const& dir: m_directories) {
            for (auto const& id: dir.default_applications(mime_type)) {
                if (contains(assocs, id)) {
                    result = load_desktop_entry_(m_directories, id);
                    if (result) return result;
                }
                else {
                    log::warn() << make_association_error(mime_type, dir, id);
                }
            }
        }

        for (auto& [id, _]: assocs) {
            result = load_desktop_entry_(m_directories, id);
            if (result) break;
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<desktop_entry> database::impl::
find_default_application(string_view mime_type,
                         error_set& errors) const
{
    try {
        opt<desktop_entry> result;

        auto const& assocs = get_associations_for_relevant_mime_types(
                                             mime_db(), m_directories, mime_type);
        if (assocs.empty()) {
            return {};
        }

        for (auto const& dir: m_directories) {
            for (auto const& id: dir.default_applications(mime_type)) {
                if (contains(assocs, id)) {
                    result = load_desktop_entry_(m_directories, id, errors);
                    if (result) return result;
                }
                else {
                    auto e = make_association_error(mime_type, dir, id);
                    auto ep = std::make_exception_ptr(std::move(e));
                    st9::push_back(errors, ep);
                }
            }
        }

        for (auto& [id, _]: assocs) {
            result = load_desktop_entry_(m_directories, id, errors);
            if (result) break;
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

database::desktop_entry_set database::impl::
all_applications() const
{
    try {
        database::desktop_entry_set result;
        unique_array<string> id_index;

        for (auto it = m_directories.begin(); it != m_directories.end(); ++it) {
            auto const& dir = *it;
            dir.all_applications(
                [&](auto ent) {
                    string id = ent.id();

                    if (rng::contains(id_index, id)) return;

                    st9::push_back(result, std::move(ent));

                    id_index.insert(id);
                });
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

unique_array<string_view> database::impl::
associated_mime_types(string_view prefix) const
{
    try {
        unique_array<string_view> result;

        for (auto const& dir: m_directories) {
            dir.associated_mime_types(prefix,
                [&](auto&& mt) {
                    result.insert(mt);
                } );
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<string_view> database::impl::
data_directory_paths() const
{
    try {
        array<string_view> result;

        for (auto const& dir: m_directories) {
            st9::push_back(result, dir.path());
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<desktop_entry> database::impl::
load_desktop_entry(string_view id) const noexcept
{
    return load_desktop_entry_(m_directories, id);
}

void database::impl::
delete_directory(directory& d) noexcept
{
    st9::erase_if(m_directories, [&](auto& e) { return &e == &d; });
}

opt<directory&> database::impl::
scan_directory_for_desktop_entry(string_view id) noexcept
{
    using path::operator/;

    for (auto& dir: m_directories) {
        try {
            auto path = dir.path() / "applications" / id;

            if (env().file_exists(path)) {
                return dir;
            }

            if (str::contains(id, "-")) {
                auto name = string(id);
                str::find_replace_all(name, "-", "/");

                auto path = dir.path() / "applications" / name;

                if (env().file_exists(path)) {
                    return dir;
                }
            }
        }
        catch (...) {
            print_error(log::err());
        }
    }

    return {};
}

opt<string> database::impl::
scan_path_for_desktop_entry(string_view id)
{
    try {
        auto s = desktop_entry_path(m_directories, id);
        if (s.empty()) {
            return {};
        }
        else {
            return s;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

mime::database const& database::impl::
mime_db() const noexcept
{
    if (m_external_mime_db) {
        return *m_external_mime_db;
    }
    else {
        return m_internal_mime_db;
    }
}

/*
 * database
 */
database::
database()
    try : m_p {}
{}
catch (...) {
    rethrow_error();
}

database::
database(mime::database const& db)
    try : m_p { node<impl>(db) }
{}
catch (...) {
    rethrow_error();
}

database::database(database&&) noexcept = default;
database& database::operator=(database&&) noexcept = default;

database::~database() noexcept = default;

database::preference_ordered_desktop_entry_set database::
find_applications(string_view mime_type) const
{
    try {
        return m_p->find_applications(mime_type);
    }
    catch (...) {
        rethrow_error();
    }
}

opt<desktop_entry> database::
find_default_application(string_view mime_type) const
{
    try {
        return m_p->find_default_application(mime_type);
    }
    catch (...) {
        rethrow_error();
    }
}

opt<desktop_entry> database::
find_default_application(string_view mime_type,
                         error_set& errors) const
{
    try {
        return m_p->find_default_application(mime_type, errors);
    }
    catch (...) {
        rethrow_error();
    }
}

database::desktop_entry_set database::
all_applications() const
{
    try {
        return m_p->all_applications();
    }
    catch (...) {
        rethrow_error();
    }
}

unique_array<string_view> database::
associated_mime_types(string_view prefix/*= {}*/) const
{
    try {
        return m_p->associated_mime_types(prefix);
    }
    catch (...) {
        rethrow_error();
    }
}

array<string_view> database::
data_directory_paths() const
{
    try {
        return m_p->data_directory_paths();
    }
    catch (...) {
        rethrow_error();
    }
}

opt<desktop_entry> database::
load_desktop_entry(string_view id) const noexcept
{
    return m_p->load_desktop_entry(id);
}

void database::
add_association(string_view mime_type, string_view id)
{
    try {
        auto& mime_apps_list = m_p->config_home().mime_apps_list();

        mime_apps_list.add_association(mime_type, id);
    }
    catch (...) {
        rethrow_error();
    }
}

void database::
remove_association(string_view mime_type, string_view id)
{
    try {
        auto& mime_apps_list = m_p->config_home().mime_apps_list();

        mime_apps_list.remove_association(mime_type, id);
    }
    catch (...) {
        rethrow_error();
    }
}

void database::
add_default_application(string_view mime_type, string_view id)
{
    try {
        auto& mime_apps_list = m_p->config_home().mime_apps_list();

        mime_apps_list.add_default_application(mime_type, id);
    }
    catch (...) {
        rethrow_error();
    }
}

void database::
remove_default_application(string_view mime_type, string_view id)
{
    try {
        auto& mime_apps_list = m_p->config_home().mime_apps_list();

        mime_apps_list.remove_default_application(mime_type, id);
    }
    catch (...) {
        rethrow_error();
    }
}

void database::
save()
{
    try {
        auto& mime_apps_list = m_p->config_home().mime_apps_list();

        mime_apps_list.save();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::applications::v1
