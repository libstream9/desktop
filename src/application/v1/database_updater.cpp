#include <stream9/xdg/application/v1/database_updater.hpp>

#include "database_p.hpp"
#include "directory.hpp"

#include <stream9/bits/predicate.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/less.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/log.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/projection.hpp>
#include <stream9/ref.hpp>
#include <stream9/strings/append.hpp>
#include <stream9/strings/ends_with.hpp>
#include <stream9/strings/find_replace.hpp>
#include <stream9/unique_array.hpp>

namespace stream9::xdg::applications::v1 {

static std::uint32_t
watch_mask() noexcept
{
    return static_cast<std::uint32_t>(
        IN_CLOSE_WRITE | IN_MOVED_TO
         | IN_DELETE | IN_MOVED_FROM
         | IN_MOVE_SELF | IN_DELETE_SELF );
}

struct database_updater::impl
{
    struct watch {
        int wd;
        enum { data, app } type;
        ref<directory> dir;
        string relpath;
    };

    database_updater& m_parent;
    database::impl& m_p;
    lx::inotify m_inotify;
    unique_array<watch, less, project<&watch::wd>> m_watches;
    array<ref<database_updater::event_handler>> m_handlers;

    impl(database_updater& parent, database::impl& p)
        try : m_parent { parent }
            , m_p { p }
            , m_inotify { IN_NONBLOCK }
    {
        for (auto& dir: m_p.directories()) {
            try {
                watch_data_directory(dir);
                watch_application_directory(dir);
            }
            catch (...) {
                print_error(log::warn());
            }
        }
    }
    catch (...) {
        rethrow_error();
    }

    ~impl() noexcept
    {
        for (auto& h: m_handlers) {
            h->m_src = nullptr;
        }
    }

    lx::fd_ref monitor_fd() const noexcept { return m_inotify.fd(); }

    void
    watch_data_directory(directory& dir)
    {
        try {
            auto wd = m_inotify.add_watch(dir.path(), watch_mask());

            m_watches.insert({
                wd, watch::data, dir, ""
            });
        }
        catch (...) {
            using enum lx::errc;

            auto ec = current_error_code();
            if (ec == enoent || ec == eacces) {
                // nop
            }
            else {
                rethrow_error();
            }
        }
    }

    opt<int>
    add_watch(string_view path)
    {
        try {
            auto wd = m_inotify.add_watch(path, watch_mask());
            return wd;
        }
        catch (...) {
            using enum lx::errc;

            auto ec = current_error_code();
            if (ec == enoent || ec == eacces) {
                return {};
            }
            else {
                rethrow_error();
            }
        }
    }

    void
    watch_application_directory(directory& dir, string_view relpath = "")
    {
        using path::operator/;

        try {
            auto path = dir.path() / "applications" / relpath;
            auto o_wd = add_watch(path);
            if (!o_wd) return;

            m_watches.insert({ //TODO remove watch if this fails maybe?
                *o_wd, watch::app, dir, relpath
            });

            lx::directory d { path }; //TODO proper directory_walker
            for (auto& ent: d) {
                auto n = lx::name(ent);
                if (n.empty()) continue;
                if (n[0] == '.') continue;

                if (lx::is_directory(ent, d.fd())) {
                    watch_application_directory(dir, relpath / n);
                }
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    unwatch_directory(int wd) noexcept
    {
        auto it = m_watches.find(wd);
        m_watches.erase(it);
    }

    string
    get_desktop_entry_id(string_view relpath, string_view filename)
    {
        string id;

        if (!relpath.empty()) {
            id = relpath;
            str::find_replace_all(id, "/", "-");
            str::append(id, '-');
        }

        str::append(id, filename);

        return id;
    }

    bool
    process_application_directory_event(directory& d, auto& ev, string_view relpath)
    {
        using path::operator/;

        try {
            auto n = lx::name(ev);

            if (n == "mimeinfo.cache") {
                d.reload_mime_info_cache();
                return true;
            }
            else if (bits::any_of(ev.mask, IN_DELETE_SELF | IN_MOVE_SELF)) {
                if (relpath.empty()) {
                    m_p.delete_directory(d); //TODO ignore subdirectory
                }
                else {
                    //TODO check if IN_DELETE is delivered to files in the directory
                }

                return true;
            }
            else if (str::ends_with(n, ".desktop")) {
                auto id = get_desktop_entry_id(relpath, n);

                auto& entries = m_p.entries();

                if (bits::any_of(ev.mask, IN_CLOSE_WRITE | IN_MOVED_TO)) {
                    auto entry_path = d.path() / relpath / n;

                    auto it = entries.find(id);
                    if (it == entries.end()) { // new desktop entry
                        entries.emplace(id, d.path() / relpath);

                        for (auto& h: m_handlers) {
                            h->desktop_entry_added(id, entry_path);
                        }
                    }
                    else { // one of desktop entries for id has changed.
                        auto o_dir = m_p.scan_directory_for_desktop_entry(id);
                        if (o_dir) {
                            if (&*o_dir == &d) { // highest priority desktop entry has changed.
                                for (auto& h: m_handlers) {
                                    h->desktop_entry_changed(id, entry_path);
                                }
                            }
                            else {
                                // ignore change on lower priority entry
                            }
                        }
                        else {
                            //TODO this shouldn't happen.
                        }
                    }

                    return true;
                }
                else if (bits::any_of(ev.mask, IN_DELETE | IN_MOVED_FROM)) { // delete
                    auto it = entries.find(id);
                    if (it == entries.end()) {
                        //TODO this shouldn't happen.
                    }
                    else {
                        auto o_path = m_p.scan_path_for_desktop_entry(id);
                        if (o_path) { // there is a desktop entry for id in a lower priority directory.
                            it->value = *o_path;

                            for (auto& h: m_handlers) {
                                h->desktop_entry_changed(id, *o_path);
                            }
                        }
                        else {
                            entries.erase(it);

                            for (auto& h: m_handlers) {
                                h->desktop_entry_deleted(id);
                            }
                        }
                    }

                    return true;
                }
            }

            return false;
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool
    process_data_directory_event(directory& d, auto& ev)
    {
        try {
            auto n = lx::name(ev);

            if (str::ends_with(n, "mimeapps.list") )
            {
                d.reload_mime_apps_list(n);
                return true;
            }

            return false;

        }
        catch (...) {
            rethrow_error();
        }
    }

    bool
    process_inotify_event(auto& ev) noexcept
    {
        try {
            auto it = m_watches.find(ev.wd);
            if (it == m_watches.end()) return false;

            if (bits::any_of(ev.mask, IN_IGNORED)) {
                unwatch_directory(ev.wd);
                return false;
            }

            if (it->type == watch::app) {
                return process_application_directory_event(it->dir, ev, it->relpath);
            }
            else if (it->type == watch::data) {
                return process_data_directory_event(it->dir, ev);
            }
        }
        catch (...) {
            print_error(log::warn());
        }

        return false;
    }

    int
    process_events()
    {
        try {
            int num_processed = 0;
            char buf[1024];

            while (true) {
                auto events = m_inotify.read(buf);
                if (events.empty()) break;

                for (auto const& ev: events) {
                    if (process_inotify_event(ev)) {
                        ++num_processed;
                    }
                }
            }

            return num_processed;
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    add_event_handler(database_updater::event_handler& h)
    {
        m_handlers.insert(h);
        h.m_src = this;
    }

    void
    remove_event_handler(database_updater::event_handler& h) noexcept
    {
        st9::erase_if(m_handlers, [&](auto& e) { return &e == &h; });
    }
};

/*
 * class database_updater
 */
database_updater::
database_updater(database& p)
    try : m_p { *this, p.m_p }
{}
catch (...) {
    rethrow_error();
}

database_updater::~database_updater() noexcept = default;

lx::fd_ref database_updater::
monitor_fd() const noexcept
{
    return m_p->monitor_fd();
}

safe_integer<int, 0> database_updater::
process_events()
{
    try {
        return m_p->process_events();
    }
    catch (...) {
        rethrow_error();
    }
}

void database_updater::
add_event_handler(event_handler& h)
{
    try {
        m_p->add_event_handler(h);
    }
    catch (...) {
        rethrow_error();
    }
}

database_updater::event_handler::
~event_handler() noexcept
{
    if (m_src) {
        m_src->remove_event_handler(*this);
    }
}

} // namespace stream9::xdg::applications::v1
