#ifndef STREAM9_XDG_APPLICATION_DIRECTORY_HPP
#define STREAM9_XDG_APPLICATION_DIRECTORY_HPP

#include <stream9/xdg/desktop_entry.hpp>
#include <stream9/xdg/error.hpp>

#include "../id_set.hpp"
#include "../mime_apps_list.hpp"
#include "../mime_info_cache.hpp"

#include "../../namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/function_ref.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::applications {

class directory
{
public:
    using desktop_file_id_list = array<string_view, 10>;

public:
    directory(string_view path);

    // accessor
    cstring_view path() const noexcept { return m_path; }

    auto& mime_apps_list() noexcept { return m_mime_apps_list; }

    auto const& desktop_mime_apps_lists() const noexcept { return m_desktop_mime_apps_lists; }

    // query
    desktop_file_id_list installed_applications(string_view mime_type) const;

    id_set const& added_associations(string_view mime_type) const;
    id_set const& removed_associations(string_view mime_type) const;

    id_set default_applications(string_view mime_type) const;

    void all_applications(function_ref<void(desktop_entry)>) const;

    void associated_mime_types(string_view prefix,
                               function_ref<void(string_view)>) const;

    // modifier
    void reload_mime_info_cache();
    void reload_mime_apps_list(string_view filename);

private:
    string m_path;
    mime_info_cache m_mime_info_cache;
    class mime_apps_list m_mime_apps_list;
    array<class mime_apps_list> m_desktop_mime_apps_lists;
};

} // namespace stream9::xdg::applications

#endif // STREAM9_XDG_APPLICATION_DIRECTORY_HPP
